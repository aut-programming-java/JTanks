/*** In The Name of Allah ***/
package game.Basis;

import java.awt.*;

/**
 * This Interface has Draw Method
 * For Drawing Each of the images
 * that for Drawing Each Object
 * ,it should implements This Class
 * @author Mazochi & Khatemi
 */
public interface Drawable {

    public abstract void draw(Graphics2D g2d, ImageCreator imageCreator);
}
