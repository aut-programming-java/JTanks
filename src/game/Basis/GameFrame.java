/*** In The Name of Allah ***/
package game.Basis;

import game.Barrier.Barrier;
import game.Decoration.Decoration;
import game.Shot.PresentableShot;
import game.Tank.PresentableTank;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import javax.swing.JFrame;

/**
 * This class is created for drawing all of the frame of play
 * with bufferStrategy with the method Draw for each of them.
 *
 *    http://docs.oracle.com/javase/tutorial/extra/fullscreen/bufferstrategy.html
 *    http://docs.oracle.com/javase/8/docs/api/java/awt/image/BufferStrategy.html
 * 
 * @author Seyed Mohammad Ghaffarian & Mazochi & Khatemi
 */
public class GameFrame extends JFrame {
	
	public static final int GAME_HEIGHT = 720;                  // 720p game resolution
	public static final int GAME_WIDTH = 16 * GAME_HEIGHT / 9;  // wide aspect ratio

	private BufferStrategy bufferStrategy;

	private ImageCreator imageCreator=new ImageCreator();

	public GameFrame(String title) {
		super(title);
		setResizable(false);
		setSize(GAME_WIDTH, GAME_HEIGHT);
	}
	
	/**
	 * This must be called once after the JFrame is shown:
	 *    frame.setVisible(true);
	 * and before any rendering is started.
	 */
	public void initBufferStrategy() {
		createBufferStrategy(3);
		bufferStrategy = getBufferStrategy();
	}

	/**
	 * Game rendering with triple-buffering using BufferStrategy.
	 */
	public void render(GameState state) {
		// Render single frame
		do {
			// The following loop ensures that the contents of the drawing buffer
			// are consistent in case the underlying surface was recreated
			do {
				// Get a new graphics context every time through the loop
				// to make sure the strategy is validated
				Graphics2D graphics = (Graphics2D) bufferStrategy.getDrawGraphics();
				try {
					doRendering(graphics, state);
				} finally {
					// Dispose the graphics
					graphics.dispose();
				}
				// Repeat the rendering if the drawing buffer contents were restored
			} while (bufferStrategy.contentsRestored());

			// Display the buffer
			bufferStrategy.show();
			// Tell the system to do the drawing NOW;
			// otherwise it can take a few extra ms and will feel jerky!
			Toolkit.getDefaultToolkit().sync();

		// Repeat the rendering if the drawing buffer was lost
		} while (bufferStrategy.contentsLost());
	}
	
	/**
	 * Rendering all game elements based on the game state.
	 */
	private void doRendering(Graphics2D g2d, GameState state) {
		// Draw background
		g2d.setColor(Color.DARK_GRAY);
		g2d.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);

		 // Draw All
		if(state.getStatus().equals("Menu")){
			state.getMenu().draw(g2d,imageCreator);
		}
		else if(state.getStatus().equals("Game")){

			for(Decoration decoration:state.getPresentableBackGrounds()){
				decoration.draw(g2d,imageCreator);
			}

			for(PresentableTank presentableTank:state.getPresentableTanks()){
				presentableTank.draw(g2d,imageCreator);
			}

			for(PresentableShot presentableShot:state.getPresentableShots()){
				presentableShot.draw(g2d,imageCreator);

			}
			for(Barrier barrier:state.getPresentableBarriers()){
				barrier.draw(g2d,imageCreator);
			}

			if (state.getEffects().size() != 0) {
				Effect effect = state.getEffects().get(0);
				effect.draw(g2d,imageCreator);
				ArrayList<Effect> effectsGroup = state.getEffects();
				effectsGroup.remove(effect);
				state.setEffects(effectsGroup);

			}

			for (Trophy trophy : state.getPresentableTrophies()){
				trophy.draw(g2d,imageCreator);
			}
			for (Decoration decoration:state.getPresentableForeGrounds()){
				decoration.draw(g2d,imageCreator);
			}

			state.getInformationPanel().draw(g2d,imageCreator);

		}

		// Draw GAME OVER
		if (state.isGameOver()) {
			String str = "GAME OVER";
			g2d.setColor(Color.BLACK);
			g2d.setFont(g2d.getFont().deriveFont(Font.BOLD).deriveFont(64.0f));
			int strWidth = g2d.getFontMetrics().stringWidth(str);
			g2d.drawImage(ImageCreator.readStatic("gameOver.png"),0,0,null);
			g2d.drawString(str, (GAME_WIDTH - strWidth) / 2, GAME_HEIGHT / 2);
		}

		// Draw Win
		if (state.isWin()) {
			String str = "YOU ARE WIN!";
			g2d.setColor(Color.WHITE);
			g2d.setFont(g2d.getFont().deriveFont(Font.BOLD).deriveFont(64.0f));
			int strWidth = g2d.getFontMetrics().stringWidth(str);
			g2d.drawImage(ImageCreator.readStatic("win.png"),0,0,null);
			g2d.drawString(str, (GAME_WIDTH - strWidth) / 2, GAME_HEIGHT / 2);
		}
	}
	
}
