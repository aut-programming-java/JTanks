/*** In The Name of Allah ***/
package game.Basis;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

/**
 * A very simple structure for the main game loop.
 * THIS IS NOT PERFECT, but works for most situations.
 * Note that to make this work, none of the 2 methods 
 * in the while loop (update() and render()) should be 
 * long running! Both must execute very quickly, without 
 * any waiting and blocking!
 * 
 * Detailed discussion on different game loop design
 * patterns is available in the following link:
 *    http://gameprogrammingpatterns.com/game-loop.html
 * 
 * @author Seyed Mohammad Ghaffarian & Mazochi & Khatemi
 */
public class GameLoop implements Runnable {
	
	/**
	 * Frame Per Second.
	 * Higher is better, but any value above 24 is fine.
	 */
	public static final int FPS = 30;
	
	private GameFrame canvas;
	private GameState state;
	private KeyListener keyListener;
	private MouseListener mouseListener;

	private double delayTime;

	public GameLoop(GameFrame frame) {
		canvas = frame;
	}
	
	/**
	 * This must be called before the game loop starts.
	 */
	public void init() {
		state = new GameState();
		keyListener = state.getKeyListener();
		canvas.addKeyListener(keyListener);
		mouseListener = state.getMouseListener();
		canvas.addMouseListener(mouseListener);
		canvas.addMouseMotionListener(null);
	}

	@Override
	public void run() {
		boolean finishGame = false;
		while (!finishGame) {
			try {
				long start = System.currentTimeMillis();
				state.update();
				canvas.removeKeyListener(keyListener);
				keyListener = state.getKeyListener();
				canvas.addKeyListener(keyListener);
				canvas.removeMouseListener(mouseListener);
				mouseListener = state.getMouseListener();
				canvas.addMouseListener(mouseListener);
				canvas.render(state);
				if(state.isFinishedGame()){
					if (delayTime==0){
						delayTime = System.nanoTime();
					}
					else{
						if (System.nanoTime()-delayTime>1000*1000*1000){
							finishGame=true;
						}

					}
				}
//				finishGame = state.isFinishedGame();
				//
				long delay = (1000 / FPS) - (System.currentTimeMillis() - start);
				if (delay > 0)
					Thread.sleep(delay);
			} catch (InterruptedException ex) {
			}
		}
		canvas.render(state);
	}
}
