/*** In The Name of Allah ***/
package game.Basis;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * This class Get the String of the pathname
 * and return the image of it and its Unique
 * Because of HashMap
 * @author Mazochi & Khatemi
 */
public class ImageCreator {
    HashMap<String , BufferedImage> images;

    public ImageCreator(){
        images = new HashMap<>();
    }

    /**
     * This Method Create a image
     * if you don't have it before that
     * but if you have it before it
     * this method get you the previous image
     * @param address
     * @return image that you want
     */
    public  BufferedImage read(String address){
        if(images.containsKey(address)){
            return images.get(address);
        }
        else{
            try{
                BufferedImage bufferedImage;
                bufferedImage= ImageIO.read(new File(address));
                images.put(address,bufferedImage);
                return bufferedImage;
            }
            catch(IOException e){
                System.out.println(e);
            }
            return null;
        }
    }

    /**
     * This class always create the image and
     * it's not important the previous images
     * @param address
     * @return the image that you want
     */
    public static BufferedImage readStatic (String address){
        try{
            BufferedImage bufferedImage;
            bufferedImage= ImageIO.read(new File(address));
            return bufferedImage;
        }
        catch(IOException e){
            System.out.println(e);
        }
        return null;
    }
}
