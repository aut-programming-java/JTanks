/*** In The Name of Allah ***/
package game.Basis;

import game.Barrier.Barrier;
import game.Barrier.HardWall;
import game.Barrier.SoftWall;
import game.Barrier.Teazel;
import game.Decoration.BackGround;
import game.Decoration.Decoration;
import game.Decoration.ForeGround;
import game.Network.Client;
import game.Network.ClientHandler;
import game.Network.Server;
import game.Shot.PresentableShot;
import game.Shot.Shot;
import game.Tank.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class holds the state of game and all of its elements.
 * This class also handles user inputs, which affect the game state.
 * 
 * @author Seyed Mohammad Ghaffarian & khatami & Mazochi
 *
 */
public class GameState{

	private OwnTank ownTank;
	private OwnTank ownTank2;


	private InformationPanel informationPanel;

	private ArrayList<PresentableTank> presentableTanks;
	private ArrayList<PresentableShot> presentableShots;
	private ArrayList<Barrier> presentableBarriers;
	private ArrayList<Decoration> presentableBackGrounds;
	private ArrayList<Decoration> presentableForeGrounds;
	private ArrayList<Trophy> presentableTrophies;

	private ArrayList<Tank> tanks;
	private ArrayList<Shot> shots;
	private ArrayList<Barrier> barriers;
	private ArrayList<Effect> effects;

	private ArrayList<Decoration> backGrounds;
	private ArrayList<Decoration> foreGrounds;

	private ArrayList<Trophy> trophies;

	private String status;
    private Menu menu;

    private int originX=0;
    private int originY=0;

    public static final int MAP_HEIGHT=69;
    public static final int MAP_WIDTH=39;

	private String networkStatus;
	private Client client;
	private Server server;
	private ClientHandler clientHandler;

    private boolean gameOver;
    private boolean win;

	private boolean soundGamePlaying;
	private SoundPlayer soundPlayer;

	private String level;
	private int stage;
	private final int NUMBER_OF_STAGES=3;

	private OwnTank previousOwnTank1;
	private OwnTank previousOwnTank2;

	private boolean soundEndGame;

	private double delayTime;

	private boolean finishedGame;

	/**
	 * Constructor for class GameState
	 */
	public GameState() {
		gameOver = false;
		win=false;
		soundGamePlaying=false;

		stage=1;
		level="Easy";

		tanks=new ArrayList<>();
		shots=new ArrayList<>();
		barriers=new ArrayList<>();
		backGrounds=new ArrayList<>();
		foreGrounds=new ArrayList<>();
		trophies = new ArrayList<>();
		effects = new ArrayList<>();

		presentableBarriers=new ArrayList<>();
		presentableShots=new ArrayList<>();
		presentableTanks=new ArrayList<>();
		presentableBackGrounds=new ArrayList<>();
		presentableForeGrounds=new ArrayList<>();
		presentableTrophies = new ArrayList<>();

		menu=new Menu();
		status="Menu";

	}

	/**
	 *
	 * @param name name of file
	 * @return a table of characters in file
	 */
	public char[][] readOfFile(String name) {
		String[] strings = new String[MAP_HEIGHT];
		File txtFile = new File(name);
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(txtFile));
			int i = 0;
			while (reader.ready()) {
				String line = reader.readLine();
				strings[i] = line;
				i++;
			}
		} catch (IOException ex) {
			System.err.println(ex);
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException ex) {
				System.err.println(ex);
			}
		}

		char[][] map = new char[MAP_HEIGHT][MAP_WIDTH];

		for (int i = 0 ; i < MAP_HEIGHT ; i++){
			map[i] = strings[i].toCharArray();
		}

		return map;
	}

	/**
	 * Make map with a name
	 * @param nameMap name of three text
	 */
	public void makeMap(String nameMap){
		char[][] mainGround;
		char[][] backGround;
		char[][] foreGround;

		mainGround = readOfFile(nameMap + "_MainGround.txt");
		backGround = readOfFile(nameMap + "_BackGround.txt");
		foreGround = readOfFile(nameMap + "_ForeGround.txt");



		makeMainGround(mainGround);
		makeBackGrounds(backGround);
		makeForeGrounds(foreGround);

    }

	/**
	 * Make object about main of any map
	 * @param map a array of character
	 */
	public void makeMainGround(char[][] map){
		previousOwnTank1=ownTank;
		previousOwnTank2=ownTank2;

		barriers.clear();
		tanks.clear();
		trophies.clear();

		for (int i = 0; i < MAP_HEIGHT ; i++){
			for (int j = 0 ; j < MAP_WIDTH ; j++) {
				if(map[i][j]=='O'){
						ownTank = new OwnTank(j * 100 ,i * 100,this,1,previousOwnTank1);
						tanks.add(ownTank);
					informationPanel = new InformationPanel(ownTank);
				}
				if(map[i][j]=='o'){
					if(networkStatus.equals("Server")) {
						ownTank2 = new OwnTank(j * 100, i * 100, this, 2,previousOwnTank2);
						tanks.add(ownTank2);
					}
				}
			}
		}
		int life=0;

		for (int i = 0; i < MAP_HEIGHT ; i++){
			for (int j = 0 ; j < MAP_WIDTH ; j++) {
				if (level==null){
					level="Easy";
				}
				switch (map[i][j]){
					case 'M':
						switch (level){
							case "Easy":
								life=10;
								break;
							case "Normal":
								life=20;
								break;
							case "Hard":
								life=30;
								break;
						}

						EnemyTank enemyTank1 = new MovableEnemyTank(ownTank,ownTank2
								,j * 100,i * 100,life,this,0);
						tanks.add(enemyTank1);
						break;
                    case 'm':
						switch (level){
							case "Easy":
								life=10;
								break;
							case "Normal":
								life=10;
								break;
							case "Hard":
								life=20;
								break;
						}
                        EnemyTank enemyTank2 = new MovableEnemyTank(ownTank,ownTank2
                                ,j * 100,i * 100,life,this,1);
                        tanks.add(enemyTank2);
                        break;
					case 'N':
						switch (level){
							case "Easy":
								life=20;
								break;
							case "Normal":
								life=30;
								break;
							case "Hard":
								life=40;
								break;
						}
						EnemyTank enemyTank3 = new MovableEnemyTank(ownTank,ownTank2
								,j * 100,i * 100,life,this,2);
						tanks.add(enemyTank3);
						break;
					case 'n':
						switch (level){
							case "Easy":
								life=40;
								break;
							case "Normal":
								life=50;
								break;
							case "Hard":
								life=80;
								break;
						}
						EnemyTank enemyTank4 = new MovableEnemyTank(ownTank,ownTank2
								,j * 100,i * 100,life,this,3);
						tanks.add(enemyTank4);
						break;
					case 'I':
						switch (level){
							case "Easy":
								life=10;
								break;
							case "Normal":
								life=20;
								break;
							case "Hard":
								life=20;
								break;
						}

						EnemyTank enemyTankImmovable = new ImmovableEnemyTank(ownTank,ownTank2
								,j * 100,i * 100,life,this,0);
						tanks.add(enemyTankImmovable);
						break;
					case 'i':
						switch (level){
							case "Easy":
								life=20;
								break;
							case "Normal":
								life=30;
								break;
							case "Hard":
								life=40;
								break;
						}

						EnemyTank enemyTankImmovable2 = new ImmovableEnemyTank(ownTank,ownTank2
								,j * 100,i * 100,life,this,1);
						tanks.add(enemyTankImmovable2);
						break;
					case 'H':
						if(stage==1){
							barriers.add(new HardWall(j * 100,i * 100 , "Wall"));
						}
						else if(stage==2){
							barriers.add(new HardWall(j * 100,i * 100 , "WallP"));
						}
						else if(stage==3){
							barriers.add(new HardWall(j * 100,i * 100 , "WallR"));
						}
						break;
					case 'K':
						barriers.add(new HardWall(j * 100,i * 100 , "RKapsool"));
						break;
					case 'k':
						barriers.add(new HardWall(j * 100,i * 100 , "LKapsool"));
						break;
					case '1':
						if(stage==1){
							barriers.add(new HardWall(j * 100,i * 100 , "Pipe1"));
						}
						else{
							barriers.add(new HardWall(j * 100,i * 100 , "PipeR1"));
						}
						break;
					case '2':
						if(stage==1){
							barriers.add(new HardWall(j * 100,i * 100 , "Pipe2"));
						}
						else{
							barriers.add(new HardWall(j * 100,i * 100 , "PipeR2"));
						}
						break;
					case '3':
						barriers.add(new HardWall(j * 100,i * 100 , "Pipe3"));
						break;
					case '4':
						barriers.add(new HardWall(j * 100,i * 100 , "Pipe4"));
						break;
					case '5':
						barriers.add(new HardWall(j * 100,i * 100 , "Pipe5"));
						break;
					case '6':
						barriers.add(new HardWall(j * 100,i * 100 , "Pipe6"));
						break;
					case 'S':
						if(stage==1) {
							barriers.add(new SoftWall(j * 100,i * 100,"Brown"));
						}
						else if(stage==2){
							barriers.add(new SoftWall(j * 100,i * 100,"Purple"));
						}
						else{
							barriers.add(new SoftWall(j * 100,i * 100,"Red"));
						}
						break;
					case 'T':
						barriers.add(new Teazel(j * 100,i * 100));
						break;
					case 'C':
						trophies.add(new Trophy(j * 100 , i * 100 , "CannonFood"));
						break;
					case 'L':
						trophies.add(new Trophy(j * 100 , i * 100 , "RepairFood"));
						break;
					case 'G':
						trophies.add(new Trophy(j * 100 , i * 100 , "MachineGunFood"));
						break;
					case 'D':
						trophies.add(new Trophy(j * 100 , i * 100 , "Star"));
						break;
					case 'E':
						break;
				}
			}
		}


	}

	/**
	 * Make objects about back ground
	 * @param backGround a array of caracter
	 */
	public void makeBackGrounds(char[][] backGround){
		backGrounds.clear();

		for (int i = 0; i < MAP_HEIGHT ; i++){
			for (int j = 0 ; j < MAP_WIDTH ; j++) {
				switch (backGround[i][j]){
					case 'S':
						backGrounds.add(new BackGround(j * 100,i * 100,"Soil",stage));
						break;
					case 'R':
						backGrounds.add(new BackGround(j * 100,i * 100,"Rail",stage));
						break;
					case 'G':
						backGrounds.add(new BackGround(j * 100,i * 100,"Granite",stage));
						break;
					case 'E':
						break;
				}
			}
		}
	}

	/**
	 * Make object about fore ground
	 * @param foreGround a array of character
	 */
	public void makeForeGrounds(char[][] foreGround){
		foreGrounds.clear();

		for (int i = 0; i < MAP_HEIGHT ; i++){
			for (int j = 0 ; j < MAP_WIDTH ; j++) {
				switch (foreGround[i][j]){
					case 'T':
						foreGrounds.add(new ForeGround(j * 100,i * 100,"Tree"));
						break;
					case 'V':
						foreGrounds.add(new ForeGround(j * 100,i * 100,"VFens"));
						break;
					case 'H':
						foreGrounds.add(new ForeGround(j * 100,i * 100,"HFens"));
						break;
					case 'P':
						foreGrounds.add(new ForeGround(j * 100,i * 100,"ForeGroundP"));
						break;
					case 'R':
						foreGrounds.add(new ForeGround(j * 100,i * 100,"ForeGroundR"));
						break;
					case '1':
						foreGrounds.add(new ForeGround(j * 100,i * 100,"Pipe1"));
						break;
					case '2':
						foreGrounds.add(new ForeGround(j * 100,i * 100,"Pipe2"));
						break;
					case '3':
						foreGrounds.add(new ForeGround(j * 100,i * 100,"Pipe3"));
						break;
					case '4':
						foreGrounds.add(new ForeGround(j * 100,i * 100,"Pipe4"));
						break;
					case '5':
						foreGrounds.add(new ForeGround(j * 100,i * 100,"Pipe5"));
						break;
					case '6':
						foreGrounds.add(new ForeGround(j * 100,i * 100,"Pipe6"));
						break;
					case 'E':
						break;
				}
			}
		}
	}

	/**
	 * This method play background sound
	 */
	public void playSoundGame(){
		if(soundGamePlaying == false && !networkStatus.equals("Client")) {
			soundGamePlaying = true;
			soundEndGame = false;
			switch (stage){
				case 1:
					soundPlayer = new SoundPlayer("gameSound1.wav");
					break;
				case 2:
            		soundPlayer = new SoundPlayer("startup.wav");
					break;
				case 3:
					soundPlayer = new SoundPlayer("endOfGame.wav");
					break;
			}
            soundPlayer.play();
		}
	}

	/**
	 * a setter for origin
	 * @param x
	 * @param y
	 */
    public void changeOrigin(int x,int y){
		originX = x;
		originY = y;
	}

	/**
	 * The method which updates the game state.
	 */
	public void update() {
        switch (status){
            case "Menu":
                showMenu();
                break;
            case "Game":
                updateGame();
                break;
        }
	}

	/**
	 * This method show menu
	 */
	public void showMenu(){
	    if(menu.getStatus()==3){
	        status="Game";
	        level=menu.getLevel();
	        networkStatus = menu.getPlayerGameType();
			switch (stage) {
				case 1:
					makeMap("Map1");
					break;
				case 2:
					makeMap("Map2");
					break;
				case 3:
					makeMap("Map3");
					break;
			}
	        switch (networkStatus){
				case "Single":
					client = null;
					server = null;
					break;
				case "Server":
					client = null;
					server = new Server(this,ownTank2);
					break;
				case "Client" :
					clientHandler = new ClientHandler();
					client = new Client(this,clientHandler);
					server = null;
					break;
			}
        }
    }

	/**
	 * a getter for status
	 * @return menu or game
	 */
    public String getStatus() {
        return status;
    }

	/**
	 * getter for menu
	 * @return a object that show menu of game
	 */
	public Menu getMenu() {
        return menu;
    }

	/**
	 * This method use for update game
	 */
	public void updateGame(){
		playSoundGame();


		if(networkStatus.equals("Server")){
			server.setOwnTank(ownTank2);
			server.updateServer();
		}
		if(networkStatus.equals("Client")){
			client.updateClient();
		}

        if(networkStatus.equals("Single") || networkStatus.equals("Server")){
			removeInActiveShots();
			removeDeadTanks();
			removeDestroyedBarrier();

			collideShots();
			collideTrophies();

			checkGameFinish();

			presentableTanks.clear();
			for(Tank tank:tanks){
				if(tank.makePresentable(originX,originY)){
					if(!tank.equals(ownTank2)){
						tank.update();
						presentableTanks.add(new PresentableTank(tank));
					}
				}
			}

			if(networkStatus.equals("Server")){
				ownTank2.makePresentable(originX,originY);
				presentableTanks.add(new PresentableTank(ownTank2));
			}


			presentableShots.clear();
			ArrayList<Shot> removedShots = new ArrayList<>();
			for(Shot shot:shots){
				if(shot.makePresentable(originX,originY)){
					shot.update();
					presentableShots.add(new PresentableShot(shot));
				}
				else{
					removedShots.add(shot);
				}
			}
			for (Shot shot : removedShots){
				shots.remove(shot);
			}

			presentableBarriers.clear();
			for(Barrier barrier : barriers){
				if(barrier.makePresentable(originX,originY)){
					presentableBarriers.add(barrier);
				}
			}

			presentableBackGrounds.clear();
			for(Decoration decoration:backGrounds){
				if(decoration.makePresentable(originX,originY)){
					presentableBackGrounds.add(decoration);
				}
			}

			presentableForeGrounds.clear();
			for(Decoration decoration:foreGrounds){
				if(decoration.makePresentable(originX,originY)){
					presentableForeGrounds.add(decoration);
				}
			}

			presentableTrophies.clear();
			for(Trophy trophy:trophies){
				if(trophy.makePresentable(originX,originY)){
					presentableTrophies.add(trophy);
				}
			}

			gotoNextLevel();

		}



    }

	/**
	 * This method checks shot collides to barrier or tanks
	 */
	public void collideShots(){
		for(Shot shot:shots){
			for(Tank tank: tanks){
				tank.collideShot(shot);
			}
		}
		for(Shot shot:shots){
		    for(Barrier barrier:barriers){
		        barrier.collideShot(shot);
		        if (barrier instanceof SoftWall){
		        	SoftWall softWall=(SoftWall)barrier;
		        	if(softWall.getChangeImage()){
		        		Effect effect = null;
		        		SoundPlayer.play("softwall.wav");
		        		switch (stage){
							case 1:
								effect =new Effect("7.png",barrier.getRealX(),barrier.getRealY());
								break;
							case 2:
								effect =new Effect("8.png",barrier.getRealX(),barrier.getRealY());
								break;
							case 3:
								effect =new Effect("9.png",barrier.getRealX(),barrier.getRealY());
								break;

						}
						effect.makePresentable(originX,originY);
		        		effects.add(effect);
					}
				}
            }
        }
	}

	/**
	 * This method check ownTank or all enemy tanks died or no
	 */
	public void checkGameFinish(){
	    boolean flagEnemyTank = false;
	    for(Tank tank:tanks){
	        if(tank instanceof EnemyTank){
	            flagEnemyTank =true;
	            break;
            }
        }
        if(flagEnemyTank == false ){
            win = true;
        }

        if(ownTank.isAlive() == false || (ownTank2!=null && ownTank2.isAlive()==false)){
            gameOver = true;
        }
    }

	/**
	 * This method check ownTank recieves trophies or no
	 */
	public void collideTrophies(){
		for(Trophy trophy : trophies){
			for(Tank tank: tanks){
				if (tank instanceof OwnTank){
					if (((OwnTank) tank).collideTrophy(trophy)){
						trophies.remove(trophy);
						collideTrophies();
						return;
					}
				}
			}
		}
	}

	/**
	 * remove all shot that collide to other objects
	 */
	public void removeInActiveShots(){
		boolean flag=true;
		while(flag){
			flag=false;
			for(Shot shot:shots){
				if(!shot.isActive()){
					shots.remove(shot);
					flag=true;
					break;
				}
			}
		}
	}

	/**
	 * This method removed barreirs that destroyed
	 */
	public void removeDestroyedBarrier(){
        boolean flag=true;
        while(flag){
            flag=false;
            for(Barrier barrier:barriers){
                if(!barrier.isAlive()){
                    barriers.remove(barrier);
					backGrounds.add(new BackGround(barrier.getRealX() , barrier.getRealY() , "DestroyWall",stage));
					flag=true;
                    break;
                }
            }
        }
    }

	/**
	 * This method removed tanks that destroyed
	 */
	public void removeDeadTanks(){
		boolean flag=true;
		while(flag){
			flag=false;
			for(Tank tank : tanks){
				if(!tank.isAlive()){
					SoundPlayer.play("enemydestroyed.wav");
					tanks.remove(tank);
					if(tank instanceof EnemyTank){
						generateDeadTank(tank);
					}
					flag=true;
					break;
				}
			}
		}
	}

	/**
	 * When a enemy tank died generate a background and generate a random trophy
	 * @param tank a tank that destroyed
	 */
	public void generateDeadTank(Tank tank){
		Random random = new Random();
		int randomNumber=random.nextInt(100);

		effects.add(new Effect("1.png",tank.getRealX(),tank.getRealY()));
		effects.add(new Effect("2.png",tank.getRealX(),tank.getRealY()));
		effects.add(new Effect("3.png",tank.getRealX(),tank.getRealY()));
		effects.add(new Effect("4.png",tank.getRealX(),tank.getRealY()));
		effects.add(new Effect("5.png",tank.getRealX(),tank.getRealY()));
		effects.add(new Effect("6.png",tank.getRealX(),tank.getRealY()));
		for (Effect effect : effects) {
			effect.makePresentable(originX, originY);
		}

		backGrounds.add(new BackGround(tank.getRealX() , tank.getRealY() , "Destroy",stage));
		if(randomNumber<75){
			return;
		}
		else if (randomNumber < 78){
			trophies.add(new Trophy(tank.getRealX(),tank.getRealY(),"Chance"));
		}
		else if(randomNumber<82){
			trophies.add(new Trophy(tank.getRealX(),tank.getRealY(),"Star"));
		}
		else if(randomNumber<86){
			trophies.add(new Trophy(tank.getRealX(),tank.getRealY(),"RepairFood"));
		}
		else if(randomNumber<93){
			trophies.add(new Trophy(tank.getRealX(),tank.getRealY(),"MachineGunFood"));
		}
		else {
			trophies.add(new Trophy(tank.getRealX(),tank.getRealY(),"CannonFood"));
		}
	}

	/**
	 * give a tank and locate of tank
	 * @param x locX of tank
	 * @param y locY of tank
	 * @param tank
	 * @return is collide?
	 */
	public boolean collideBarriers(int x,int y,Tank tank){
		int width=tank.getTankWidth(), height=tank.getTankHeight();
		for (Barrier barrier : barriers){
			if (barrier.isCollide(x+5,y+5)) {
				return true;
			}
			if (barrier.isCollide(x + width -5, y+5)){
				return true;
			}
			if (barrier.isCollide(x+5,y + height-5)){
				return true;
			}
			if (barrier.isCollide(x + width -5 , y + height -5)){
				return true;
			}
		}

		if(tank instanceof  OwnTank){
			if(tank.isInMonitor(x,y,originX,originY) == false){
				return true;
			}
		}

		for(Tank otherTank: tanks){
			if(!tank.equals(otherTank)) {
				if (otherTank.isCollide(x+5, y+5)) {
					return true;
				}
				if (otherTank.isCollide(x + width-5 , y+5 )) {
					return true;
				}
				if (otherTank.isCollide(x+5 , y + height-5 )) {
					return true;
				}
				if (otherTank.isCollide(x + width -5, y + height -5)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * getter for presentableTrophies
	 * @return presentableTrophies
	 */
	public ArrayList<Trophy> getPresentableTrophies() {
		return presentableTrophies;
	}

	/**
	 * getter for ownTank
	 * @return ownTank
	 */
	public OwnTank getOwnTank() {
		return ownTank;
	}

	/**
	 * getter for ownTank2
	 * @return ownTank2
	 */
	public OwnTank getOwnTank2() {
		return ownTank2;
	}

	/**
	 * getter for keyListener
	 * @return active keyListener
	 */
	public KeyListener getKeyListener() {
	    if(status.equals("Menu")){
	        return menu.getKeyHandler();
        }
        else{
	    	if(networkStatus.equals("Client")){
	    		return clientHandler.getKeyHandler();
			}
			else{
		    	return ownTank.getKeyListener();

			}
        }
	}

	/**
	 * getter for mouseListener
	 * @return active mouseListener
	 */
	public MouseListener getMouseListener() {
		if(networkStatus==null){
//			return ownTank.getMouseListener();
			return null;
		}

		if(networkStatus.equals("Client")){
			return clientHandler.getMouseHandler();
		}
		else{
			return ownTank.getMouseListener();
		}
	}

	/**
	 * getter for network Status
	 * @return network Status
	 */
	public String getNetworkStatus() {
		return networkStatus;
	}

	/**
	 * if game finished or no
	 * this event in two case: 1) game lost 2) player finish all level
	 * @return result
	 */
    public boolean isFinishedGame() {
    	if(finishedGame){
			SoundPlayer.play("super_hero.wav");
			soundPlayer.stop();
			soundEndGame=true;
    		return true;
		}
        if(gameOver || (win && stage == NUMBER_OF_STAGES + 1) ) {
                if (soundEndGame==false && gameOver ) {
                    SoundPlayer.play("gameover.wav");
                    soundPlayer.stop();
                    soundEndGame=true;
                }
                else{
//					SoundPlayer.play("win.wav");
					soundPlayer.stop();
					soundEndGame=true;
				}
                return  true;
        }
        return false;
    }

	/**
	 * When a level finished this method automatically load next level
	 */
	public void gotoNextLevel(){
		if(win){
		    if (delayTime==0){
		        delayTime = System.nanoTime();
            }
            else{
		        if(System.nanoTime()-delayTime>10l*1000*1000*1000){
                    stage++;
                    if(stage==NUMBER_OF_STAGES+1){
                    	finishedGame=true;
					}
                    if(stage==2){
                        makeMap("Map2");
                    }
					if(stage==3){
						makeMap("Map3");
					}
					if(soundEndGame==false){
//                    	SoundPlayer.play("win.wav");
                    	soundPlayer.stop();
                    	soundGamePlaying=false;
                    	soundEndGame = true;
					}
					else{

					}
					win=false;
                }
            }

		}
	}

	/**
	 * getter for gameOver
	 * @return gameOver
	 */
    public boolean isGameOver() {
        return gameOver;
    }

	/**
	 * getter for win
	 * @return win
	 */
	public boolean isWin() {
        return win;
    }

	/**
	 * getter for presentableTanks
	 * @return presentableTanks
	 */
	public ArrayList<PresentableTank> getPresentableTanks() {
		return presentableTanks;
	}

	/**
	 * getter for presentableShots
	 * @return presentableShots
	 */
	public ArrayList<PresentableShot> getPresentableShots() {
		return presentableShots;
	}

	/**
	 * getter for presentableBarriers
	 * @return presentableBarriers
	 */
	public ArrayList<Barrier> getPresentableBarriers() {
		return presentableBarriers;
	}

	/**
	 * getter for effects
	 * @return effects
	 */
	public ArrayList<Effect> getEffects() {
		return effects;
	}

	/**
	 * getter for presentableBackGrounds
	 * @return presentableBackGrounds
	 */
	public ArrayList<Decoration> getPresentableBackGrounds() {
		return presentableBackGrounds;
	}

	/**
	 * getter for presentableForeGrounds
	 * @return presentableForeGrounds
	 */
	public ArrayList<Decoration> getPresentableForeGrounds() {
		return presentableForeGrounds;
	}

	/**
	 * getter for informationPanel
	 * @return informationPanel
	 */
	public InformationPanel getInformationPanel() {
		return informationPanel;
	}

	/**
	 * add a new shot to shots
	 * @param shot
	 */
	public void addShot(Shot shot){
		shots.add(shot);

	}

	/**
	 * getter for presentableTanks
	 * @param presentableTanks
	 */
	public void setPresentableTanks(ArrayList<PresentableTank> presentableTanks) {
		this.presentableTanks = presentableTanks;
	}

	/**
	 * setter for presentableTrophies
	 * @param presentableTrophies
	 */
	public void setPresentableTrophies(ArrayList<Trophy> presentableTrophies) {
		this.presentableTrophies = presentableTrophies;
	}

	/**
	 * setter for presentableShots
	 * @param presentableShots
	 */
	public void setPresentableShots(ArrayList<PresentableShot> presentableShots) {
		this.presentableShots = presentableShots;
	}

	/**
	 * setter for presentableBarriers
	 * @param presentableBarriers
	 */
	public void setPresentableBarriers(ArrayList<Barrier> presentableBarriers) {
		this.presentableBarriers = presentableBarriers;
	}

	/**
	 * setter for effects
	 * @param effects
	 */
	public void setEffects(ArrayList<Effect> effects) {
		this.effects = effects;
	}

	/**
	 * setter for
	 * @param presentableBackGrounds presentableBackGrounds
	 */
	public void setPresentableBackGrounds(ArrayList<Decoration> presentableBackGrounds) {
		this.presentableBackGrounds = presentableBackGrounds;
	}

	/**
	 * setter for presentableForeGrounds
	 * @param presentableForeGrounds
	 */
	public void setPresentableForeGrounds(ArrayList<Decoration> presentableForeGrounds) {
		this.presentableForeGrounds = presentableForeGrounds;
	}

	/**
	 * setter tanks
	 * @param tanks
	 */
	public void setTanks(ArrayList<Tank> tanks) {
		this.tanks = tanks;
	}

	/**
	 * setter for shots
	 * @param shots
	 */
	public void setShots(ArrayList<Shot> shots) {
		this.shots = shots;
	}

	/**
	 * setter for barriers
	 * @param barriers
	 */
	public void setBarriers(ArrayList<Barrier> barriers) {
		this.barriers = barriers;
	}

	/**
	 * setter background
	 * @param backGrounds
	 */
	public void setBackGrounds(ArrayList<Decoration> backGrounds) {
		this.backGrounds = backGrounds;
	}

	/**
	 * setter foreGrounds
	 * @param foreGrounds
	 */
	public void setForeGrounds(ArrayList<Decoration> foreGrounds) {
		this.foreGrounds = foreGrounds;
	}
}

