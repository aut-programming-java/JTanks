package game.Basis;

/*** In The Name of Allah ***/
import game.Tank.OwnTank;

/**
 * This Class is For Some Code that we Enter with Shift
 * and we Can Repair Our health
 * full degree of our Gun
 * full number of our gun to 4000
 * and we can be Game Over
 * @author Mazochi & Khatemi
 */
public class FakeCode{

    private OwnTank ownTank;
    private String code;

    public FakeCode(OwnTank ownTank){
        this.ownTank = ownTank;
        code = ownTank.getCode();

    }

    /**
     * This Method is Checked it that
     * our Codes Are available or not... .
     */
    public void checkCode(){
        if (code != null){
            if (code.equals("health") || code.equals("HEALTH")){
                ownTank.setLife(50);
                ownTank.setCode("");
            }
            else if ("HEALTH".startsWith(code) || "health".startsWith(code)){
                System.out.println(code);
            }
            else if (code.equals("fulldegree") || code.equals("FULLDEGREE")){
                for(int i=0; i<5 ; i++){
                    ownTank.levelUp();
                }
                ownTank.setCode("");
            }
            else if ("fulldegree".startsWith(code) || "FULLDEGREE".startsWith(code)){
                System.out.println(code);
            }
            else if (code.equals("fullgun") || code.equals("FULLGUN")){
                ownTank.setNumberOfCannon(4000);
                ownTank.setNumberOfGun(4000);
                ownTank.setCode("");
            }
            else if ("fullgun".startsWith(code) || "FULLGUN".startsWith(code)){
                System.out.println(code);
            }
            else if (code.equals("dead") || code.equals("DEAD")){
                ownTank.setAlive(false);
                ownTank.setCode("");
            }
            else if ("dead".startsWith(code) || "DEAD".startsWith(code)){
                System.out.print("");
            }
            else {
                ownTank.setCode("");
            }


        }


    }
}

