/*** In The Name of Allah ***/
package game.Basis;

import java.awt.*;
import java.io.Serializable;

/**
 * This class is about Drawing trophies that we have in play and after some dead tanks
 * these are +200 MachineGun , +40 Cannon , Repair Your Health , Add Chances
 * and Upgrade the Gun that you Use ... .
 *
 * @author Mazochi & Khatemi
 */
public class Trophy implements Drawable,Serializable {

    private int realX;
    private int realY;
    private int locX;
    private int locY;
    private String image;
    private String type;

    /**
     * We Recognize Each Trophy with type Of it
     * that we read from constructor
     * and we can have the picture of it
     * @param realX
     * @param realY
     * @param type
     */
    public Trophy(int realX , int realY , String type){
        this.realX = realX;
        this.realY = realY;
        this.type = type;
        if (type.equals("RepairFood")){
            image = "RepairFood.png";
        }
        else if (type.equals("MachineGunFood")){
            image = "MachinGunFood.png";
        }
        else if (type.equals("CannonFood")){
            image = "CannonFood.png";
        }
        else if (type.equals("Star")){
            image = "BigStar.png";
        }
        else if (type.equals("Chance")){
            image = "ChanceTrophy.png";
        }

    }

    /**
     * Get x Of Origin and y Of Origin and set the location of Trophies
     * And if can Draw it return True if not return False
     * @param originX x Of Origin
     * @param originY y Of Origin
     * @return Boolean
     */
    public boolean makePresentable(int originX , int originY ){
        locX=realX-originX;
        locY=realY-originY;
        if(-100 < locX && locX <= 1280 && -100< locY && locY <= 720){
            return true;
        }
        else{
            return false;
        }

    }

    //Getters
    public int getRealX() {
        return realX;
    }
    public int getRealY() {
        return realY;
    }
    public String getType() {
        return type;
    }

    /**
     * For Drawing the picture Of Trophy that we have
     * @param g2d
     * @param imageCreator
     */
    @Override
    public void draw(Graphics2D g2d, ImageCreator imageCreator) {
        g2d.drawImage(imageCreator.read(image),locX,locY,null);
    }
}
