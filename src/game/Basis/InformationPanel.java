/*** In The Name of Allah ***/
package game.Basis;

import game.Tank.OwnTank;
import java.awt.*;
import java.io.Serializable;

/**
 * This class is the Draw Of the Panel of
 * health , Number And Degree Of each gun
 * And Chances in the above of the frame... .
 * @author Mazochi & Khatemi
 */
public class InformationPanel implements Drawable, Serializable{
    private String starImage;
    private String canonImage;
    private String machineGunImage;
    private String healthFullImage;
    private String healthEmptyImage;
    private String chance;
    private OwnTank ownTank;

    public InformationPanel(OwnTank ownTank){
        this.ownTank = ownTank;
        canonImage = "NumberOfHeavyBullet.png";
        machineGunImage = "NumberOfMachinGun.png";
        healthFullImage = "health.png";
        healthEmptyImage = "healthEmpty.png";
        starImage = "Star.png";
        chance = "Chance.png";
    }

    /**
     * Drawing this Panel With this Method
     * @param g2d
     * @param imageCreator
     */
    @Override
    public void draw(Graphics2D g2d,ImageCreator imageCreator) {
       drawGun(g2d,imageCreator);
       drawHealth(g2d,imageCreator);
       drawStars(g2d,imageCreator);
       drawChances(g2d,imageCreator);
    }

    /**
     * Drawing the Number
     * and picture of each Gun.
     * @param g2d
     * @param imageCreator
     */
    public void drawGun(Graphics2D g2d,ImageCreator imageCreator){

        if (ownTank.getTypeAttack().contains("Cannon")) {
            g2d.drawImage(imageCreator.read("Sign.png"),12,30,null);
        }
        else if (ownTank.getTypeAttack().contains("MachineGun")){
            g2d.drawImage(imageCreator.read("Sign.png"),12,92,null);
        }
        g2d.drawImage(imageCreator.read(canonImage),8,30,null);
        g2d.drawImage(imageCreator.read(machineGunImage),10,90,null);

        g2d.setFont(new Font("Arial",Font.BOLD,25));

        if(ownTank.getNumberOfCannon()==0){
            g2d.setColor(Color.RED);
        }
        else{
            g2d.setColor(Color.GREEN);
        }
        g2d.drawString(makeNumberString(ownTank.getNumberOfCannon()), 55, 85);


        if(ownTank.getNumberOfGun()==0){
            g2d.setColor(Color.RED);
        }
        else{
            g2d.setColor(Color.GREEN);
        }
        g2d.drawString(makeNumberString(ownTank.getNumberOfGun()), 55, 145);

    }

    /**
     * Draw the number of Empty And Full Healths.
     * @param g2d
     * @param imageCreator
     */
    public void drawHealth(Graphics2D g2d, ImageCreator imageCreator){
        g2d.drawImage(imageCreator.read(healthFullImage),500,40,null);

        if(ownTank.getLife()>10){
            g2d.drawImage(imageCreator.read(healthFullImage),550,40,null);
        }
        else{
            g2d.drawImage(imageCreator.read(healthEmptyImage),550,40,null);
        }

        if(ownTank.getLife()>20){
            g2d.drawImage(imageCreator.read(healthFullImage),600,40,null);
        }
        else{
            g2d.drawImage(imageCreator.read(healthEmptyImage),600,40,null);
        }

        if(ownTank.getLife()>30){
            g2d.drawImage(imageCreator.read(healthFullImage),650,40,null);
        }
        else{
            g2d.drawImage(imageCreator.read(healthEmptyImage),650,40,null);
        }

        if(ownTank.getLife()>40){
            g2d.drawImage(imageCreator.read(healthFullImage),700,40,null);
        }
        else{
            g2d.drawImage(imageCreator.read(healthEmptyImage),700,40,null);
        }

    }

    /**
     * Drawing the Stars for
     * Grade Of each Gun.
     * @param g2d
     * @param imageCreator
     */
    private  void drawStars(Graphics2D g2d,ImageCreator imageCreator){
        if (ownTank.getDegreeOfCanon() > 0){
            g2d.drawImage(imageCreator.read(starImage),8,25,null);
        }
        if (ownTank.getDegreeOfCanon() > 1){
            g2d.drawImage(imageCreator.read(starImage),8,35,null);
        }
        if (ownTank.getDegreeOfCanon() > 2){
            g2d.drawImage(imageCreator.read(starImage),8,45,null);
        }

        if (ownTank.getDegreeOfGun() > 0){
            g2d.drawImage(imageCreator.read(starImage),10,85,null);
        }
        if (ownTank.getDegreeOfGun() > 1){
            g2d.drawImage(imageCreator.read(starImage),10,95,null);
        }

    }

    /**
     * Draw the pictures about
     * How many Chances do you have
     * @param g2d
     * @param imageCreator
     */
    public void drawChances(Graphics2D g2d , ImageCreator imageCreator){
        if (ownTank.getChance() > 0){
            g2d.drawImage(imageCreator.read(chance),1240,28,null);
        }
        if (ownTank.getChance() > 1){
            g2d.drawImage(imageCreator.read(chance),1210,28,null);
        }
        if (ownTank.getChance() > 2){
            g2d.drawImage(imageCreator.read(chance),1180,28,null);
        }
        if (ownTank.getChance() > 3){
            g2d.drawImage(imageCreator.read(chance),1150,28,null);
        }
        if (ownTank.getChance() > 4){
            g2d.drawImage(imageCreator.read(chance),1120,28,null);
        }

    }

    /**
     * For Making the String Of Number Of Each Gun
     * @param number
     * @return
     */
    public String makeNumberString(int number){
        String str=String.format(""+number);
        switch (str.length()){
            case 1:
                str="0"+str;
            case 2:
                str="0"+str;
            case 3:
                str="0"+str;
                break;
        }
        return  str;
    }


}
