/*** In The Name of Allah ***/
package game.Basis;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.Serializable;

/**
 * This class For Showing the menu
 * and SElect the Level And Status
 * For Playing Single,Server Or Client
 *
 * @author Mazochi & Khatemi
 */
public class Menu implements Drawable, Serializable{

    private String startupImage;
    private String startup2Image;
    private String startup3Image;
    private String startupStarImage;
    private KeyHandler keyHandler;

    private int locationStar;
    private int locationStar2;
    private int status;

    private String level;
    private String playerGameType;

    public Menu(){
        locationStar = 0;
        locationStar2 = 0;
        keyHandler=new KeyHandler();
        status=0;
        startupImage = "Startup.png";
        startup2Image = "Startup2.png";
        startup3Image = "Startup3.png";
        startupStarImage = "StartupStar.png";
    }

    /**
     * Draw the images Of Menu With different Cases
     * @param g2d
     * @param imageCreator
     */
    @Override
    public void draw(Graphics2D g2d, ImageCreator imageCreator) {
        switch (status){
            case 0:
                g2d.drawImage(imageCreator.readStatic(startupImage),0,25,null);
                g2d.drawImage(imageCreator.readStatic(startupStarImage),170,320+locationStar*50,null);
                break;
            case 1:
                if (locationStar != 2) {
                    g2d.drawImage(imageCreator.readStatic(startup3Image), 0, 25, null);
                    g2d.drawImage(imageCreator.readStatic(startupStarImage), 170, 320 + locationStar2 * 50, null);
                    break;
                }
                else {
                    status =2;
                }
            case 2:
                g2d.drawImage(imageCreator.readStatic(startup2Image), 0, 25, null);
                break;
            case 3:
                break;
        }



    }

    //Getters
    public int getStatus() {
        return status;
    }
    public KeyHandler getKeyHandler() {
        return keyHandler;
    }
    public String getLevel() {
        return level;
    }
    public String getPlayerGameType() {
        return playerGameType;
    }

    /**
     * This Key Listener is For Choosing
     * the status and level of play
     */
    private class KeyHandler extends KeyAdapter implements Serializable{
        @Override
        public void keyPressed(KeyEvent e) {
            SoundPlayer.play("select.wav");
            switch (status){
                case 2:
                    status=3;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                break;
                case 1:
                    if(e.getKeyCode()==KeyEvent.VK_DOWN){
                        locationStar2++;
                        locationStar2%=3;
                    }
                    else if (e.getKeyCode()==KeyEvent.VK_UP){
                        locationStar2--;
                        if(locationStar2<0){
                            locationStar2=2;
                        }
                    }
                    else if(e.getKeyCode()==KeyEvent.VK_ENTER){
                        switch (locationStar2){
                            case 0:
                                level = "Easy";
                                break;
                            case 1:
                                level = "Normal";
                                break;
                            case 2:
                                level = "Hard";
                                break;
                        }
                        status=2;
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                    break;
                case 0:
                    if(e.getKeyCode()==KeyEvent.VK_DOWN){
                        locationStar++;
                        locationStar%=3;
                    }
                    else if (e.getKeyCode()==KeyEvent.VK_UP){
                        locationStar--;
                        if(locationStar<0){
                            locationStar=2;
                        }
                    }
                    else if(e.getKeyCode()==KeyEvent.VK_ENTER){
                        switch (locationStar){
                            case 0:
                                playerGameType="Single";
                                break;
                            case 1:
                                playerGameType="Server";
                                break;
                            case 2:
                                playerGameType="Client";
                                break;
                        }
                        status=1;
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                    break;
            }
        }
    }
}
