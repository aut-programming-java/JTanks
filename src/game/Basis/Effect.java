/*** In The Name of Allah ***/
package game.Basis;

import java.awt.*;
import java.io.Serializable;

/**
 * This Class is For Effects Of After Dead Tanks
 * and Effect For Destroying SoftWalls
 * @author Mazochi & Khatemi
 */
public class Effect implements Serializable,Drawable{

    private String image;
    private int realX , realY;
    private int locX , locY;

    public Effect(String image , int realX , int realY){
        this.image = image;
        this.realX = realX;
        this.realY = realY;
    }

    /**
     * Get x Of Origin and y Of Origin and set the location of Effect
     * And if can Draw it return True if not return False
     * @param originX x Of Origin
     * @param originY y Of Origin
     * @return Boolean
     */
    public boolean makePresentable(int originX , int originY ) {
        locX = realX - originX;
        locY = realY - originY;
        if (-100 < locX && locX <= 1280 && -100 < locY && locY <= 720) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * For Drawing the Image Of it
     * @param g2d
     * @param imageCreator
     */
    @Override
    public void draw(Graphics2D g2d, ImageCreator imageCreator) {
        g2d.drawImage(imageCreator.read(image),locX,locY,null);
    }
}
