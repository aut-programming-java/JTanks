/*** In The Name of Allah ***/
package game.Basis;// Java program to play an Audio
// file using Clip Object
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.io.*;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * This class can play a sound with two way
 * a static way that play a sound one times
 * and other that use for play background sound with advance option :)
 *
 * @author Mazochi & Khatemi
 */
public class SoundPlayer
{

    // to store current position
    private Long currentFrame;
    private Clip clip;

    // current status of clip
    private String status;

    private AudioInputStream audioInputStream;
    private String filePath;


    // constructor to initialize streams and clip
    public SoundPlayer(String filePath) {
        try {
            this.filePath = filePath;
            // create AudioInputStream object
            audioInputStream =
                    AudioSystem.getAudioInputStream(new File(filePath).getAbsoluteFile());

            // create clip reference
            clip = AudioSystem.getClip();

            // open audioInputStream to the clip
            clip.open(audioInputStream);

            clip.loop(Clip.LOOP_CONTINUOUSLY);
        }
        catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
    }

    /**
     * play music
     */
    public void play()
    {
        //start the clip
        clip.start();

        status = "play";
    }

    /**
     * pause a music
     */
    public void pause()
    {
        if (status.equals("paused"))
        {
            System.out.println("audio is already paused");
            return;
        }
        this.currentFrame =
                this.clip.getMicrosecondPosition();
        clip.stop();
        status = "paused";
    }


    /**
     * stop loop of playing music
     */
    public void stop() {
        currentFrame = 0L;
        clip.stop();
        clip.close();
    }

    /**
     * static method for playing a sound for one time
     * @param filePath path of file
     */
    public static void play(String filePath){
        {
            try{
                // open the sound file as a Java input stream
                InputStream in = new FileInputStream(filePath);

                // create an audiostream from the inputstream
                AudioStream audioStream = new AudioStream(in);

                // play the audio clip with the audioplayer class
                AudioPlayer.player.start(audioStream);
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
