/*** In The Name of Allah ***/
package game.Shot;

import game.Basis.ImageCreator;
import game.Basis.SoundPlayer;
import game.Tank.OwnTank;
import game.Tank.Tank;
import java.io.Serializable;

/**
 * This class is For Shots with Different Kinds
 * And the Destroy Method can inactive the Shot
 * and remove it and the Update method can move
 * the Shot ... .
 *
 * @author Mazochi & Khatemi
 */
public class Shot implements Serializable{
    private int power;
    private int speed;
    private Tank owner;
    private double angle;
    private int locX;
    private int locY;
    private int realX;
    private int realY;

    private ImageCreator imageCreator;

    private String image;
    private boolean active;

    public Shot(Tank owner,String type){

        this.owner = owner;
        angle = owner.getAngleOfHead();
        realX = owner.getRealX() + 30 + (int)(Math.cos(angle)*50);
        realY = owner.getRealY() + 30 + (int)(Math.sin(angle)*50);

        imageCreator = new ImageCreator();
        active = true;

        switch (type){
            case "Cannon":
                power=10;
                speed=20;
                image ="shotCannon.png";
                SoundPlayer.play("heavygun.wav");
                break;
            case "Cannon1":
                power=10;
                speed=30;
                image="shotCannon.png";
                SoundPlayer.play("heavygun.wav");
                break;
            case "Cannon2":
                power=15;
                speed=30;
                image="shotCannon.png";
                SoundPlayer.play("heavygun.wav");
                break;
            case "Cannon3":
                power=20;
                speed=30;
                image="shotCannon.png";
                SoundPlayer.play("heavygun.wav");
                break;
            case "MachineGun":
                power=2;
                speed=30;
                image = "shotMachineGun.png";
                SoundPlayer.play("mashingun.wav");
                break;
            case "MachineGun1":
                power=2;
                speed=40;
                image = "shotMachineGun.png";
                SoundPlayer.play("mashingun.wav");
                break;
            case "MachineGun2":
                power=4;
                speed=50;
                image = "shotMachineGun.png";
                SoundPlayer.play("mashingun.wav");
                break;
            case "Simple":
                power=1;
                speed=20;
                image = "lightGun.png";

                break;

        }


    }

    public void destroy(){
        active=false;
    }

    public boolean isActive(){
        return active;
    }

    /**
     * This Method Change the X and Y of shot
     * with speed and Angle of it... .
     */
    public void update(){
        realY += speed*Math.sin(angle);
        realX += speed*Math.cos(angle);
    }

    //Getters
    public int getWidth() {
        return imageCreator.read(image).getWidth();
    }
    public int getHeight() {
        return imageCreator.read(image).getHeight();
    }
    public int getPower() {
        return power;
    }
    public Tank getOwner() {
        return owner;
    }
    public double getAngle() {
        return angle;
    }
    public int getLocX() {
        return locX;
    }
    public int getLocY() {
        return locY;
    }
    public int getRealX() {
        return realX;
    }
    public int getRealY() {
        return realY;
    }
    public String getImage() {
        return image;
    }

    //Setters
    public void setLocX(int locX) {
        this.locX = locX;
    }
    public void setLocY(int locY) {
        this.locY = locY;
    }

    /**
     * Get x Of Origin and y Of Origin and set the location of Shots
     * And if can Draw it return True if not return False
     * @param originX x Of Origin
     * @param originY y Of Origin
     * @return Boolean
     */
    public boolean makePresentable(int originX , int originY ){
        locX=realX-originX;
        locY=realY-originY;
        if(-100 < locX && locX <= 1280 && -100< locY && locY <= 720){
            return true;
        }
        else{
            return false;
        }

    }

}
