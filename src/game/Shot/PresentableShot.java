/*** In The Name of Allah ***/
package game.Shot;

import game.Basis.Drawable;
import game.Basis.ImageCreator;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.io.Serializable;

/**
 * This Class is For Drawing the presentable Shots
 * and Show Them in the Stage with its Location
 *
 * @author Mazochi & Khatemi
 */
public class PresentableShot implements Drawable, Serializable {
    private int locX;
    private int locY;
    private String image;
    private double angle;

    public PresentableShot(Shot shot){
        locX = shot.getLocX();
        locY = shot.getLocY();
        image =  shot.getImage();
        angle = shot.getAngle();
    }

    /**
     * Drawing the Picture For the Specific type Of Gun
     * @param g2d
     * @param imageCreator
     */
    @Override
    public void draw(Graphics2D g2d, ImageCreator imageCreator) {
        AffineTransform tx = AffineTransform.getRotateInstance
                (angle , 20,20);
        AffineTransformOp op = new AffineTransformOp(tx , AffineTransformOp.TYPE_BILINEAR);
        g2d.drawImage(op.filter(imageCreator.read(image),null),locX ,locY ,null);
    }
}
