/*** In The Name of Allah ***/
package game.Tank;

import game.Basis.GameState;

/**
 * This is Some Enemy Tanks That can move
 * and have Four types in the Game
 *
 * @author Mazochi & Khatemi
 */
public class MovableEnemyTank extends EnemyTank implements Movable{
    private int type;

    public MovableEnemyTank(OwnTank ownTank1,OwnTank ownTank2 , int realX , int realY ,int life,
                            GameState gameState,int type){
        super(ownTank1,ownTank2,realX,realY,life,gameState);
        this.type=type;
        switch (type) {
            case 0:
                image = "BigEnemy.png";
                imageHead = "BigEnemyGun.png";
                tankHeight=100;
                tankWidth=100;
                break;
            case 1:
                image = "SmallEnemy.png";
                imageHead = "SmallEnemyGun.png";
                tankHeight=100;
                tankWidth=100;
                break;
            case 2:
                image = "BigEnemy2.png";
                imageHead = "BigEnemyGun2.png";
                tankHeight=100;
                tankWidth=100;
                break;
            case 3:
                image = "BigEnemy3.png";
                imageHead = "BigEnemyGun3.png";
                tankHeight=100;
                tankWidth=100;
                break;
        }

    }

    /**
     * This Method update Location
     * angle and the nearest OwnTank
     * For Every Movable EnemyTank
     */
    @Override
    public void update() {
        takeShot();
        calculateLocation();
        checkNearOwnTank();
        calculateAngle();
        restGun();
    }

    /**
     * Moving with out Collide to other
     * Barriers and near to Own tank
     */
    @Override
    public void moveUp() {
        if (!gameState.collideBarriers(realX , realY - 3 , this)) {
            realY = Math.max(realY-3 , nearOwnTank.realY);
        }
    }
    @Override
    public void moveDown() {
        if (!gameState.collideBarriers(realX , realY + 3 ,this)) {
            realY = Math.min(realY+3 , nearOwnTank.realY);
        }
    }
    @Override
    public void moveLeft() {
        if (!gameState.collideBarriers(realX - 3 , realY , this)) {
            realX = Math.max(realX-3 , nearOwnTank.realX);
        }
    }
    @Override
    public void moveRight() {
        if (!gameState.collideBarriers(realX + 3 , realY ,this)) {
//            realX += 3;
            realX = Math.min(realX+3,nearOwnTank.realX);
          //  locX = Math.min(locX, ownTank1.locX);
        }
    }

    /**
     * Updating Location Of Tank and can go to near the Own Tank
     */
    @Override
    public void calculateLocation() {
        if(nearOwnTank==null){
            return;
        }

        if(distance1<250 || distance2<250){
            return;
        }

        if(nearOwnTank.getRealX()<realX){
            moveLeft();
        }
        else if(nearOwnTank.getRealX()>realX){
            moveRight();
        }

        if(nearOwnTank.getRealY()<realY){
            moveUp();
        }
        else if(nearOwnTank.getRealY()>realY){
            moveDown();
        }

    }

    //Getters
    @Override
    public String getTypeAttack()
    {
        if(type ==0){
            return "Cannon";
        }
        else if (type == 1){
            return "MachineGun";
        }
        else if (type == 2){
            return "Cannon";
        }
        else {
            return "Cannon2";
        }
    }
    @Override
    public long getDelayAttack()
    {
        if(type != 1){
            return 500*1000*1000;
        }
        else{
            return 100*1000*1000;
        }
    }

}
