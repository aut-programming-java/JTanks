/*** In The Name of Allah ***/
package game.Tank;

import game.Basis.*;
import game.Shot.Shot;
import java.awt.*;
import java.awt.event.*;

/**
 * This class is about Tanks
 * that we play with them
 * and is tank of Player
 *
 * @author Mazochi & Khatemi
 */
public class OwnTank  extends Tank implements Movable {

    private KeyHandler keyHandler;
    private MouseHandler mouseHandler;

    private int degreeOfCanon;
    private int degreeOfGun;

    private int numberOfPlayer;

    private int numberOfCannon;
    private int numberOfGun;

    private int chance;

    private boolean changeGun;

    private boolean keyUP, keyDOWN, keyRIGHT, keyLEFT;
    private String typeAttack = "Cannon";

    private long timeChangeWeapon;

    private String code;
    private FakeCode fakeCode;

    public OwnTank(int realX , int realY,GameState gameState, int number, OwnTank previousOwnTank){
        super(realX,realY,50,gameState);

        numberOfCannon = 50;
        numberOfGun = 200;

        degreeOfCanon = 0;
        degreeOfGun = 0;

        chance = 2;

        if(previousOwnTank!=null){
            setPreviousInformation(previousOwnTank);
        }

        keyUP = false;
        keyDOWN = false;
        keyRIGHT = false;
        keyLEFT = false;

        attack=false;
        timeBeforeAttack=0l;
        timeChangeWeapon=0l;

        numberOfPlayer = number;

        if(number==1){
            image = "IconBody.png";
            imageHead = "IconHead.png";
        }
        else{
            image = "IconBody2.png";
            imageHead = "IconHead2.png";
        }

       tankHeight = 100;
       tankWidth = 100;

        keyHandler = new KeyHandler(this);
        mouseHandler = new MouseHandler();

        updateOrigin(this);
    }

    /**
     * This method is for information after going to another stage
     * and level to keep them e.g. Degree of guns and ... .
     * @param previousOwnTank
     */
    public void setPreviousInformation(OwnTank previousOwnTank){
        numberOfCannon = previousOwnTank.getNumberOfCannon();
        numberOfGun = previousOwnTank.getNumberOfGun();
        degreeOfCanon = previousOwnTank.getDegreeOfCanon();
        degreeOfGun = previousOwnTank.getDegreeOfGun();
        life = previousOwnTank.getLife();
        chance = previousOwnTank.getChance();
        keyHandler = (KeyHandler) previousOwnTank.getKeyListener();
        mouseHandler = (MouseHandler) previousOwnTank.getMouseListener();


    }

    /**
     * This class update Location,Angle and shot of tank , ... .
     */
    public void update() {
        updateOrigin();
        calculateLocation();
        calculateAngle();
        checkChangeGun();
        updateHeadImage();
        shot();
    }

    /**
     * This two Methods are For update
     * the origin of the page that we have shown
     * after each move in the stage... .
     */
    public void updateOrigin(){
        if(gameState.getNetworkStatus().equals("Server")){
            updateOrigin(gameState.getOwnTank2());
            return;
        }
        else {
            updateOrigin(this);
            return;
        }
    }
    public void updateOrigin(OwnTank ownTank2){
        int x,y;
        int meanX = realX + ownTank2.getRealX();
        int meanY = realY + ownTank2.getRealY();
        meanX/=2;
        meanY/=2;

        if(640>meanX){
            x=640;
        }
        else if( meanX>GameState.MAP_WIDTH * 100-640){
            x=GameState.MAP_WIDTH * 100-640;
        }
        else{
            x=meanX;
        }

        if(360>meanY){
            y=360;
        }
        else if( meanY>GameState.MAP_HEIGHT * 100-360){
            y=GameState.MAP_HEIGHT * 100-360;
        }
        else{
            y=meanY;
        }

        gameState.changeOrigin(x-640,y-360);
    }

    /**
     * Shot a kind of Shot and
     * reduce number of the kind that is shoted
     */
    public void shot(){
        if(takeShot()){
            if(typeAttack.contains("Cannon")){
                numberOfCannon--;
                if(numberOfCannon==0){
                    if(degreeOfGun>0){
                        typeAttack="MachineGun"+degreeOfGun;
                    }
                    else{
                        typeAttack="MachineGun";
                    }
                }
            }
            else if(typeAttack.contains("MachineGun")){
                numberOfGun--;
                if(numberOfGun==0){
                    if(degreeOfCanon>0){
                        typeAttack="Cannon"+degreeOfCanon;
                    }
                    else{
                        typeAttack="Cannon";
                    }
                }
            }

            if(numberOfGun==0 && numberOfCannon==0){
                typeAttack="Simple";
            }
        }
    }

    /**
     * change the image of Head
     * after upgrading the Degree
     */
    public void updateHeadImage(){
        if (numberOfPlayer == 1){
            if (typeAttack.contains("Cannon")){
                if (degreeOfCanon == 0){
                    imageHead = "IconHead.png";
                }
                else if (degreeOfCanon == 1){
                    imageHead = "IconHeadPro.png";
                }
                else if (degreeOfCanon == 2){
                    imageHead = "IconHeadProo.png";
                }
                else {
                    imageHead = "IconHeadProoo.png";
                }
            }
            else if (typeAttack.contains("MachineGun") || typeAttack.equals("Simple")){
                if (degreeOfGun == 0){
                    imageHead = "IconHeadGun.png";
                }
                else if (degreeOfGun == 1){
                    imageHead = "IconHeadGunPro.png";
                }
                else {
                    imageHead = "IconHeadGunProo.png";
                }
            }
        }
        if (numberOfPlayer == 2){
            if (typeAttack.contains("Cannon")){
                if (degreeOfCanon == 0){
                    imageHead = "IconHead2.png";
                }
                else if (degreeOfCanon == 1){
                    imageHead = "IconHeadPro2.png";
                }
                else if (degreeOfCanon == 2){
                    imageHead = "IconHeadProo2.png";
                }
                else {
                    imageHead = "IconHeadProoo2.png";
                }
            }
            else if (typeAttack.contains("MachineGun") || typeAttack.equals("Simple")){
                if (degreeOfGun == 0){
                    imageHead = "IconHeadGun2.png";
                }
                else if (degreeOfGun == 1){
                    imageHead = "IconHeadGunPro2.png";
                }
                else {
                    imageHead = "IconHeadGunProo2.png";
                }
            }
        }
    }

    /**
     * this is Like update Method But it is for Client Player
     * @param keyUP
     * @param keyDOWN
     * @param keyRIGHT
     * @param keyLEFT
     * @param attack
     * @param changeGun
     * @param mouseLocation
     */
    public void updateNetwork(Boolean keyUP,Boolean keyDOWN,Boolean keyRIGHT,Boolean keyLEFT,
                              Boolean attack, Boolean changeGun, Point mouseLocation){
        this.keyUP = keyUP;
        this.keyDOWN = keyDOWN;
        this.keyRIGHT = keyRIGHT;
        this.keyLEFT = keyLEFT;
        this.attack = attack;
        this.changeGun = changeGun;

        calculateLocation();
        calculateAngle(mouseLocation);
        checkChangeGun();
        updateHeadImage();
        shot();

    }

    /**
     * Information Of SErver & Client Playing
     * @param numberOfCannon
     * @param numberOfGun
     * @param degreeOfCanon
     * @param degreeOfGun
     * @param life
     */
    public void setNetwork(int numberOfCannon, int numberOfGun, int degreeOfCanon , int degreeOfGun, int life,int chance,String typeAttack){
        this.numberOfCannon = numberOfCannon;
        this.numberOfGun = numberOfGun ;
        this.degreeOfCanon = degreeOfCanon;
        this.degreeOfGun = degreeOfGun;
        this.life = life;
        this.chance=chance;
        this.typeAttack = typeAttack;
    }

    /**
     * This method is for changing the Gun
     * and is handled with a delay time
     */
    public void checkChangeGun(){
        if(changeGun){
            if(System.nanoTime()-timeChangeWeapon>1000*1000*1000){
                timeChangeWeapon=System.nanoTime();
                if (numberOfCannon == 0 && numberOfGun == 0) {
                    SoundPlayer.play("emptyGun.wav");
                    typeAttack="Simple";
                }
                else if (typeAttack.contains("Cannon")){
                    if(numberOfGun>0){
                        if(degreeOfGun>0){
                            typeAttack = "MachineGun"+degreeOfGun;
                        }
                        else{
                            typeAttack = "MachineGun";

                        }
                    }
                    else{
                        SoundPlayer.play("emptyGun.wav");
                    }
                }
                else{
                    if(numberOfCannon>0){
                        if(degreeOfCanon>0){
                            typeAttack = "Cannon"+degreeOfCanon;
                        }
                        else{
                            typeAttack = "Cannon";

                        }
                    }
                    else{
                        SoundPlayer.play("emptyGun.wav");
                    }
                }
            }
        }
    }

    /**
     * These Methods is For updating Angle of head of tank Always
     */
    @Override
    public void calculateAngle() {
        angleOfHead = Math.atan2(MouseInfo.getPointerInfo().getLocation().getY() - locY
                ,MouseInfo.getPointerInfo().getLocation().getX()  - locX);
        if (realX2 != realX || realY2 != realY){
            angle = Math.atan2(realX2 - realX , realY2 - realY);
        }
        else {
            angle = 0;
        }
        headLocX=50;
        headLocY=50;
    }
    public void calculateAngle(Point point) {
        angleOfHead = Math.atan2(point.getY() - locY
                ,point.getX()  - locX);
        if (keyUP || keyDOWN) {
            if (!keyLEFT && !keyRIGHT) {
                realX2 = realX;
            }
        }
        else {
            if (keyLEFT || keyRIGHT){
                realY2 = realY;
            }
        }
        angle = Math.atan2(realX2 - realX, realY2 - realY);
        headLocX=50;
        headLocY=50;
    }

    /**
     * Recognize the shot that is collide to it
     * and Destroy it and Reduce life of tank
     * @param shot
     */
    @Override
    public void collideShot(Shot shot) {
        if(isCollide(shot.getRealX(),shot.getRealY())) {
            if (shot.getOwner() instanceof EnemyTank) {
                reduceLife(shot.getPower());
                shot.destroy();
            }
        }
    }

    /**
     * This method is for getting Trophies and add the use of it
     * @param trophy
     * @return
     */
    public boolean collideTrophy(Trophy trophy) {
        if(isCollide(trophy.getRealX() + 50,trophy.getRealY() + 50) ){
            switch (trophy.getType()) {
                case "Star":
                    levelUp();
                    break;
                case "RepairFood":
                    SoundPlayer.play("repair.wav");
                    life = 50;
                    break;
                case "MachineGunFood":
                    SoundPlayer.play("repair.wav");
                    numberOfGun += 200;
                    break;
                case "CannonFood":
                    SoundPlayer.play("repair.wav");
                    numberOfCannon += 40;
                    break;
                case "Chance":
                    SoundPlayer.play("repair.wav");
                    if (chance < 5){
                        chance++;
                    }
                    break;
            }
            return true;
        }
        return false;
    }

    /**
     * this Method Reduce One Chance if your health is empty
     */
    public void reduceChance(){
        if(chance==0){
            return;
        }
        else{
            chance--;
            life=50;
            alive=true;
            degreeOfGun=0;
            degreeOfCanon=0;
            numberOfGun = Math.max(200,numberOfGun);
            numberOfCannon = Math.max(50, numberOfCannon);
            typeAttack="Cannon";

        }
    }

    /**
     * This Method Add the Degree Of Each Gun that you use it.
     */
    public void levelUp(){
        SoundPlayer.play("agree.wav");
        if (typeAttack.contains("Cannon")){
            if (degreeOfCanon < 3){
                degreeOfCanon++;
                typeAttack="Cannon"+degreeOfCanon;
            }
            else if (degreeOfGun < 2){
                degreeOfGun++;
                typeAttack="MachineGun"+degreeOfGun;
            }
        }
        else{
            if (degreeOfGun < 2){
                degreeOfGun++;
                typeAttack="MachineGun"+degreeOfGun;

            }
            else if (degreeOfCanon < 3){
                degreeOfCanon++;
                typeAttack="Cannon"+degreeOfCanon;
            }
        }
    }

    /**
     * Move with KeyHandler
     */
    @Override
    public void moveUp() {
        if (!gameState.collideBarriers(realX , realY - 8 ,this)){
            SoundPlayer.play("motor1.wav");
            realY2 = realY;
            realY -= 8;
        }
    }
    @Override
    public void moveDown() {
        if (!gameState.collideBarriers(realX , realY + 8 , this)){
            SoundPlayer.play("motor1.wav");
            realY2 = realY;
            realY += 8;
        }
    }
    @Override
    public void moveLeft() {
        if (!gameState.collideBarriers(realX - 8 , realY , this)) {
            SoundPlayer.play("motor1.wav");
            realX2 = realX;
            realX -= 8;
        }
    }
    @Override
    public void moveRight() {
        if (!gameState.collideBarriers(realX + 8 , realY , this)) {
            SoundPlayer.play("motor1.wav");
            realX2 = realX;
            realX += 8;
        }
    }

    /**
     * Updating the location of Tank after each Move
     */
    @Override
    public void calculateLocation() {
        if (keyUP)
            moveUp();
        if (keyDOWN)
            moveDown();
        if (keyLEFT)
            moveLeft();
        if (keyRIGHT)
            moveRight();
    }

    /**
     * The keyboard handler.
     */
    class KeyHandler extends KeyAdapter  {

        OwnTank ownTank;

        public KeyHandler(OwnTank ownTank){
            this.ownTank = ownTank;
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() != KeyEvent.VK_UP
                    && e.getKeyCode() != KeyEvent.VK_DOWN
                    && e.getKeyCode() != KeyEvent.VK_LEFT
                    && e.getKeyCode() != KeyEvent.VK_RIGHT
                    && e.getKeyCode() != KeyEvent.VK_ENTER
                    && e.isShiftDown()){
                    if (e.getKeyCode() != KeyEvent.VK_SHIFT) {
                        if (code != null) {
                            code = code + e.getKeyChar();
                        } else {
                            code = "" + e.getKeyChar();
                        }
                        System.out.println(code);
                        fakeCode = new FakeCode(ownTank);
                        fakeCode.checkCode();
                    }

            }

            else {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_UP:
                        keyUP = true;
                        break;
                    case KeyEvent.VK_DOWN:
                        keyDOWN = true;
                        break;
                    case KeyEvent.VK_LEFT:
                        keyLEFT = true;
                        break;
                    case KeyEvent.VK_RIGHT:
                        keyRIGHT = true;
                        break;
                    case KeyEvent.VK_W:
                        keyUP = true;
                        break;
                    case KeyEvent.VK_S:
                        keyDOWN = true;
                        break;
                    case KeyEvent.VK_A:
                        keyLEFT = true;
                        break;
                    case KeyEvent.VK_D:
                        keyRIGHT = true;
                        break;
                }
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            switch (e.getKeyCode())
            {
                case KeyEvent.VK_UP:
                    keyUP = false;
                    break;
                case KeyEvent.VK_DOWN:
                    keyDOWN = false;
                    break;
                case KeyEvent.VK_LEFT:
                    keyLEFT = false;
                    break;
                case KeyEvent.VK_RIGHT:
                    keyRIGHT = false;
                    break;
                case KeyEvent.VK_W:
                    keyUP = false;
                    break;
                case KeyEvent.VK_S:
                    keyDOWN = false;
                    break;
                case KeyEvent.VK_A:
                    keyLEFT = false;
                    break;
                case KeyEvent.VK_D:
                    keyRIGHT = false;
                    break;
            }
        }

    }

    /**
     * The mouse handler.
     */
    class MouseHandler extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            if(e.getButton()==MouseEvent.BUTTON1){
                attack=true;
            }
            if(e.getButton()==MouseEvent.BUTTON3){
                changeGun=true;
            }
        }
        @Override
        public void mouseReleased(MouseEvent e){
            attack=false;
            if(e.getButton()==MouseEvent.BUTTON3){
                changeGun=false;
            }
        }
        @Override
        public void mouseClicked(MouseEvent e) {
        }
    }

    //Setters
    public void setCode(String code) {
        this.code = code;
    }
    public void setLife(int life) {
        this.life = life;
    }
//    public void setDegreeOfCanon(int degreeOfCanon) {
//        this.degreeOfCanon = degreeOfCanon;

//    }
    public void setAlive(boolean flag){
        this.alive = flag;
    }
//    public void setDegreeOfGun(int degreeOfGun) {
//        this.degreeOfGun = degreeOfGun;
//    }
    public void setNumberOfCannon(int numberOfCannon) {
        SoundPlayer.play("repair.wav");
        this.numberOfCannon = numberOfCannon;
    }
    public void setNumberOfGun(int numberOfGun) {
        SoundPlayer.play("repair.wav");
        this.numberOfGun = numberOfGun;
    }

    //Getters
    @Override
    public String getTypeAttack() {
        return typeAttack;
    }
    @Override
    public long getDelayAttack() {
        if (typeAttack.contains("Cannon"))
            return 500*1000*1000;
        else if(typeAttack.contains("MachineGun"))
            return 100*1000*1000;
        else {
            return 100*1000*1000;
        }
    }
    public int getChance() {
        return chance;
    }
    public KeyListener getKeyListener() {
        return keyHandler;
    }
    public MouseListener getMouseListener() {
        return mouseHandler;
    }
    public String getCode() {
        return code;
    }
    public int getNumberOfCannon() {
        return numberOfCannon;
    }
    public int getNumberOfGun() {
        return numberOfGun;
    }
    public int getDegreeOfCanon() {
        return degreeOfCanon;
    }
    public int getDegreeOfGun() {
        return degreeOfGun;
    }


}
