/*** In The Name of Allah ***/
package game.Tank;

import game.Basis.*;
import game.Shot.Shot;
import java.io.Serializable;

/**
 * This class is Abstract class that have three main Subclass
 * MovableEnemyTank , ImmovableEnemyTank and OwnTank that
 * the use of each is very clear of their names.
 *
 * @author Mazochi & Khatemi
 */
abstract public class Tank implements Serializable{
    protected int locX, locY;
    protected int realX,realY;
    protected int realX2,realY2;
    protected int headLocX,headLocY;
    protected String image;
    protected String imageHead;
    protected GameState gameState;

    protected double angleOfHead;
    protected double angle;

    protected boolean alive;
    protected int life;

    protected boolean attack;
    protected Long timeBeforeAttack;

    protected int tankWidth;
    protected int tankHeight;

    public Tank(int realX , int realY ,int life , GameState gameState){
        this.realX = realX;
        this.realY = realY;
        headLocX=locX;
        headLocY=locY;
        this.gameState=gameState;
        this.life=life;
        alive = true;

        timeBeforeAttack=0l;
        attack=true;

    }

    abstract public void update();

    abstract public void calculateAngle();

    //Getters
    public String getImage() {
        return image;
    }
    public int getLocX() {
        return locX;
    }
    public boolean isAlive(){
        return alive;
    }
    public int getLocY() {
        return locY;
    }
    public int getTankHeight() {
        return tankHeight;
    }
    public int getTankWidth(){
        return tankWidth;
    }
    public double getAngle() {
        return angle;
    }
    public double getAngleOfHead() {
        return angleOfHead;
    }
    public String getImageHead() {
        return imageHead;
    }
    public int getRealX() {
        return realX;
    }
    public int getRealY() {
        return realY;
    }
    public int getLife() {
        return life;
    }

    /**
     * This Method is for Shot and
     * add A shot to Array Of Shots to Show
     * That we have a DelayTime For Handling
     * @return
     */
    public boolean takeShot(){
        if(attack){
            if(System.nanoTime()-timeBeforeAttack>getDelayAttack()){
                Shot shot=new Shot(this,getTypeAttack());
                gameState.addShot(shot);
                if(this instanceof OwnTank){
                }
                timeBeforeAttack=System.nanoTime();
                return true;
            }
        }
        return false;
    }

    /**
     * this method is for reducing life
     * with power of shot
     * @param lifeLost is power of Shot
     */
    public void reduceLife(int lifeLost){
        if(lifeLost>=life){
            life=0;
            alive=false;
            if (this instanceof OwnTank){
                ((OwnTank)this).reduceChance();
            }
        }
        else{
            life-=lifeLost;
        }
    }

    /**
     * Is For Colliding Tank with
     * Other Objects in the Stage
     * @param x X of Other Object
     * @param y Y of Other Object
     * @return True if is collide and False if isn't collide
     */
    public boolean isCollide(int x,int y){
        if((realX<=x && x<=realX + tankWidth )&&( realY <= y && y <= realY + tankHeight)){
            return true;
        }
        else{
            return false;
        }
    }

    public abstract void collideShot(Shot shot);

    /**
     * Get x Of Origin and y Of Origin and set the location of Tanks
     * And if can Draw it return True if not return False
     * @param originX x Of Origin
     * @param originY y Of Origin
     * @return Boolean
     */
    public boolean makePresentable(int originX , int originY ){
        locX=realX-originX;
        locY=realY-originY;
        if(-100 < locX && locX <= 1280 && -100< locY && locY <= 720){
            return true;
        }
        else{
            return false;
        }

    }

    /**
     * The show Of Tanks For Two Players Status... .
     * @param x
     * @param y
     * @param originX
     * @param originY
     * @return
     */
    public boolean isInMonitor(int x,int y, int originX , int originY){
        int locX=x-originX;
        int locY=y-originY;
        if(95 < locX && locX <= 1180 && 95< locY && locY <= 620){
            return true;
        }
        else{
            return false;
        }
    }

    abstract public String getTypeAttack();
    abstract public long getDelayAttack();
}
