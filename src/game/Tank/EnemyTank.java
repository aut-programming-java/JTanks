/*** In The Name of Allah ***/
package game.Tank;

import game.Basis.GameState;
import game.Shot.Shot;

/**
 * This Abstract class is for Tanks that are our Enemy
 * and we have two SubClass Of it
 * 1. is Movable
 * 2. is Immovable
 *
 * @author Mazochi & Khatemi
 */
abstract public class EnemyTank extends Tank {
    protected OwnTank ownTank1;
    protected OwnTank ownTank2;
    protected double distance1;
    protected double distance2;

    protected OwnTank nearOwnTank;

    private long restTime;

    public EnemyTank(OwnTank ownTank1,OwnTank ownTank2 , int realX , int realY ,int life, GameState gameState){
        super(realX,realY,life,gameState);
        this.ownTank1=ownTank1;
        this.ownTank2=ownTank2;
        restTime=0;

    }

    /**
     * This Method has A restTime for
     * some Delay in Shot the Gun and
     * we don't have shooting always.
     */
    public void restGun(){
        if(attack){
            if(System.nanoTime()-restTime>500*1000*1000){
                restTime=System.nanoTime();
                attack=!attack;
            }
        }
        else{
            if(System.nanoTime()-restTime>2*1000*1000*1000){
                restTime=System.nanoTime();
                attack=!attack;
            }
        }
    }

    /**
     * If We Have Two OwnTank This Tank can Find
     * the Nearest OwnTank with this Method
     */
    public void checkNearOwnTank(){
        if(ownTank1!= null && ownTank1.alive==true){
            distance1 =  Math.sqrt(Math.pow(realX-ownTank1.getRealX(),2)+Math.pow(realY-ownTank1.getRealY(),2));
        }
        else{
            distance1 = Double.MAX_VALUE;
        }

        if(ownTank2 != null && ownTank2.alive==true){
            distance2 =  Math.sqrt(Math.pow(realX-ownTank2.getRealX(),2)+Math.pow(realY-ownTank2.getRealY(),2));

        }
        else{
            distance2 = Double.MAX_VALUE;
        }

        if (ownTank1==null && ownTank2==null){
            nearOwnTank=null;
        }
        else if(distance1<distance2){
            nearOwnTank = ownTank1;
        }
        else{
            nearOwnTank = ownTank2;
        }


    }


    /**
     * Updating the Angle of Head to the nearest Own Tank with this Method
     */
    @Override
    public void calculateAngle() {
        headLocX=50;
        headLocY=50;
        if(nearOwnTank != null){
            angleOfHead = Math.atan2(nearOwnTank.getLocY() - locY
                    ,nearOwnTank.getLocX() - locX);

        }

    }

    /**
     * With this Method If Shot of Own tank
     * Collide with this kind of Enemy Tank
     * The life of tank is reduced ... .
     * @param shot the shot that we check that is collide or not
     */
    @Override
    public void collideShot(Shot shot) {
        if(isCollide(shot.getRealX(),shot.getRealY())) {
            if (shot.getOwner() instanceof OwnTank) {
                reduceLife(shot.getPower());
                shot.destroy();
            }
        }
    }


}
