/*** In The Name of Allah ***/
package game.Tank;

/**
 * This Interface is For Moving and Set Location that
 * OwnTank And Movable EnemyTanks implements this... .
 *
 * @author Mazochi & Khatemi
 */
public interface Movable {

    public void moveUp();

    public void moveDown();

    public void moveLeft();

    public void moveRight();

    public void calculateLocation();
}
