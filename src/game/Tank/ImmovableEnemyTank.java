/*** In The Name of Allah ***/
package game.Tank;

import game.Basis.GameState;

/**
 * This Class is Kind of EnemyTanks that can't move
 * and has two types of Enemy with two kinds Of Shot
 *
 * @author Mazochi & Khatemi
 */
public class ImmovableEnemyTank extends EnemyTank {
    private int type;

    public ImmovableEnemyTank(OwnTank ownTank1 , OwnTank ownTank2,int realX,int realY,int life,GameState gameState,int type){
        super(ownTank1,ownTank2,realX,realY,life,gameState);
        this.type=type;
        if (type == 0) {
            image = "ImmovableTank.png";
            imageHead = "ImmovableTankHead.png";
        }
        else {
            image = "ImmovableTank2.png";
            imageHead = "ImmovableTankHead2.png";
        }
        tankHeight=100;
        tankWidth=100;
    }

    /**
     * This Method update the Shot of EnemyTank ,
     * and find the Nearest OwnTank
     * and set the Angle of head of Tank... .
     */
    @Override
    public void update() {
        takeShot();
        checkNearOwnTank();
        calculateAngle();
        restGun();
    }

    //Getters
    @Override
    public String getTypeAttack() {
        if(type==0){
            return "MachineGun";
        }
        else{
            return "Cannon";
        }
    }
    @Override
    public long getDelayAttack()
    {
        if(type == 1){
            return 500*1000*1000;
        }
        else{
            return 100*1000*1000;
        }
    }
}
