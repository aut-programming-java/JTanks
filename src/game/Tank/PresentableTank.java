/*** In The Name of Allah ***/
package game.Tank;

import game.Basis.Drawable;
import game.Basis.ImageCreator;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.io.Serializable;

/**
 * This class is For Drawing and Showing
 * The tanks that are in the Stage Of play
 *
 * @author Mazochi & Khatemi
 */
public class PresentableTank implements Drawable,Serializable{
    private int locX,locY ;
    private String image;
    private String imageHead;
    protected double angleOfHead;
    protected double angle;

    public PresentableTank(Tank tank){
        locX = tank.getLocX();
        locY = tank.getLocY();
        image = tank.getImage();
        imageHead = tank.getImageHead();
        angleOfHead = tank.getAngleOfHead();
        angle = tank.getAngle();
    }

    /**
     * Drawing the picture Of Tank and Head Of Tank
     * @param g2d
     * @param imageCreator
     */
    @Override
    public void draw(Graphics2D g2d, ImageCreator imageCreator){
        g2d.drawImage(imageCreator.read(image) , imageCreator.read(image).getMinX()+locX ,imageCreator.read(image).getMinY()+locY  , null );

        AffineTransform tx = AffineTransform.getRotateInstance
                (angleOfHead , 50, 50);
        AffineTransformOp op = new AffineTransformOp(tx , AffineTransformOp.TYPE_BILINEAR);
        g2d.drawImage(op.filter(imageCreator.read(imageHead),null),locX,locY,null);
    }
}
