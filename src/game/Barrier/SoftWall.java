/*** In The Name of Allah ***/
package game.Barrier;

/**
 * This Class is Subclass of Barrier that is Destroyable and is Anti Shot
 * @author Mazochi & Khatemi
 */
public class SoftWall extends   Barrier {

    protected int life;
    protected boolean alive;
    private String color;
    private boolean changeImage;
    private String beforeImage;

    public SoftWall(int realX, int realY,String color){
        life=40;
        alive=true;
        this.realX = realX;
        this.realY=realY;
        this.color = color;
        update();
    }

    /**
     * This Method is For Updating the image of SoftWall
     * With the amount of life and the color of It for Each Stage... .
     */
    public void update(){
        beforeImage = image;
        if(color.equals("Brown")){
            if (30 < life) {
                image = "softWall.png";

            } else if (20 < life) {
                image = "softWall1.png";

            } else if (10 < life) {
                image = "softWall2.png";

            } else {
                image = "softWall3.png";
            }
        }
        else if (color.equals("Purple")){
            if (30 < life) {
                image = "softWallP.png";

            } else if (20 < life) {
                image = "softWallP1.png";

            } else if (10 < life) {
                image = "softWallP2.png";

            } else {
                image = "softWallP3.png";
            }
        }
        else if (color.equals("Red")){
            if (30 < life) {
                image = "softWallR.png";

            } else if (20 < life) {
                image = "softWallR1.png";

            } else if (10 < life) {
                image = "softWallR2.png";

            } else {
                image = "softWallR3.png";
            }
        }

        if(beforeImage!=null && beforeImage.equals(image)==false){
            changeImage=true;
        }
    }

    /**
     * Reduce its life with the power Of Shot that is collided to it
     * @param lifeLost is the power of shot... .
     */
    @Override
    public void reduceLife(int lifeLost){
        if(lifeLost>=life){
            life=0;
            alive=false;
        }
        else{
            life-=lifeLost;
        }
        update();
    }

    //Getters
    public boolean getChangeImage(){
        if(changeImage){
            changeImage=false;
            return true;
        }
        return false;
    }

    public boolean isAlive(){
        return alive;
    }

    @Override
    public boolean isDestroyable() {
        return true;
    }

    @Override
    public boolean isAntiShot() {
        return true;
    }
}
