/*** In The Name of Allah ***/
package game.Barrier;

/**
 * This class is SubClass Of Barrier
 * that is not Destroyable and is not Anti Shot
 * @author Mazochi & Khatemi
 */
public class Teazel extends Barrier {

    protected boolean alive;

    public Teazel(int realX , int realY){
        this.realX = realX;
        this.realY = realY;
        alive = true;
        image = "teazel.png";


    }

    @Override
    public boolean isDestroyable() {
        return false;
    }

    @Override
    public boolean isAntiShot() {
        return false;
    }

}
