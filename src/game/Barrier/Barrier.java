/*** In The Name of Allah ***/
package game.Barrier;

import game.Basis.*;
import game.Shot.Shot;

import java.awt.*;
import java.io.Serializable;

/**
 * This class is the Super Class Of Walls and Teazels that we can't cross from it ...
 * @author Mazochi & Khatemi
 */
abstract public class Barrier implements Drawable, Serializable {
    protected int locX;
    protected int locY;
    protected int realX;
    protected int realY;
    protected String image;

    //Getters
    public int getLocX() {
        return locX;
    }
    public int getLocY() {
        return locY;
    }
    public int getRealX() {
        return realX;
    }
    public int getRealY() {
        return realY;
    }

    /**
     * This method is for collide an object to a barrier
     * @param x the x of Object for colliding
     * @param y the y of Object for colliding
     * @return True if is collided & False if isn't collided
     */
    public boolean isCollide(int x, int y){
        if(realX <= x && x <= realX + 100 && realY <= y && y <= realY + 100){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * This method is for collide a shot to the barrier
     * and reduce the life of the barrier and can destroy the SoftWall
     * @param shot the shot that may collide to barrier
     */
    public void collideShot(Shot shot){
        int x=shot.getRealX(), y=shot.getRealY();
        int height=shot.getHeight();
        int width=shot.getWidth();
        if(isCollide(x+5,y+5)
                || isCollide(x+width-5,y+5)
                    || isCollide(x+5,y+height-5)
                        || isCollide(x+width-5 , y+height-5)){
            if(isAntiShot()){
                shot.destroy();
                reduceLife(shot.getPower());
            }
        }
    }

    /**
     * @return True Default
     */
    public boolean isAlive(){
        return true;
    }

    /**
     * This Methos is overwrited in Softwall subClass
     * @param lifeLost
     */
    public void reduceLife(int lifeLost){
        return;
    }

    /**
     * Is OverWriten in subclasses
     * @return Boolean
     */
    abstract public boolean isAntiShot();

    /**
     * Is OverWriten in subclasses
     * @return Boolean
     */
    abstract public boolean isDestroyable();

    /**
     * Get x Of Origin and y Of Origin and set the location of Barrier
     * And if can Draw it return True if not return False
     * @param originX x Of Origin
     * @param originY y Of Origin
     * @return Boolean
     */
    public boolean makePresentable(int originX , int originY ){
        locX=realX-originX;
        locY=realY-originY;
        if(-100 < locX && locX <= 1280 && -100 < locY && locY <= 720){
            return true;
        }
        else{
            return false;
        }

    }

    /**
     * This Method is for drawing the image of it
     * @param g2d
     * @param imageCreator
     */
    @Override
    public void draw(Graphics2D g2d,ImageCreator imageCreator) {
        g2d.drawImage(imageCreator.read(image),getLocX(),getLocY(),null);
    }
}
