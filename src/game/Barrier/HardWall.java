/*** In The Name of Allah ***/
package game.Barrier;

/**
 * This class is Subclass of Barrier that isn't Destroyable and is Anti Shot
 * @author Mazochi & Khatemi
 */
public class HardWall extends Barrier {

    protected boolean alive;

    public HardWall(int realX , int  realY , String type){
        this.realX=realX;
        this.realY=realY;
        alive = true;

        if (type.equals("Wall")){
            image = "hardWall.png";
        }
        else if(type.equals("WallP")){
            image = "hardWallP.png";
        }
        else if (type.equals("WallR")){
            image = "hardWallR.png";
        }
        else if (type.equals("RKapsool")){
            image = "RightOfKapsool.png";
        }
        else if (type.equals("LKapsool")){
            image = "LeftOfKapsool.png";
        }
        else if (type.equals("Pipe1")){
            image = "pipe1_HW.png";
        }
        else if (type.equals("PipeR1")){
            image = "pipe1_HWR.png";
        }
        else if (type.equals("Pipe2")){
            image = "pipe2_HW.png";
        }
        else if (type.equals("PipeR2")){
            image = "pipe2_HWR.png";
        }
        else if (type.equals("Pipe3")){
            image = "pipe3_HW.png";
        }
        else if (type.equals("Pipe4")){
            image = "pipe4_HW.png";
        }
        else if (type.equals("Pipe5")){
            image = "pipe5_HW.png";
        }
        else if (type.equals("Pipe6")){
            image = "pipe6_HW.png";
        }
    }

    @Override
    public boolean isDestroyable() {
        return false;
    }

    @Override
    public boolean isAntiShot() {
        return true;
    }

}
