/*** In The Name of Allah ***/
package game.Decoration;

import game.Basis.Drawable;
import game.Basis.ImageCreator;
import java.awt.*;
import java.io.Serializable;

/**
 * This abstract class is For
 * Drawing ForeGrounds And BackGrounds
 *
 * @author Mazochi & Khatemi
 */
abstract public class Decoration implements Drawable, Serializable{
    private int locX;
    private int locY;
    private int realX;
    private int realY;
    protected String image;

    public Decoration(int realX, int realY){
        this.realX=realX;
        this.realY=realY;
    }

    //Getters
    public int getLocX() {
        return locX;
    }
    public int getLocY() {
        return locY;
    }
    public int getRealX() {
        return realX;
    }
    public int getRealY() {
        return realY;
    }

    /**
     * Get x Of Origin and y Of Origin and set the location of Decorations
     * And if can Draw it return True if not return False
     * @param originX x Of Origin
     * @param originY y Of Origin
     * @return Boolean
     */
    public boolean makePresentable(int originX , int originY ){
        locX=realX-originX;
        locY=realY-originY;
        if(-100 < locX && locX <= 1280 && -100< locY && locY <= 720){
            return true;
        }
        else{
            return false;
        }

    }

    /**
     * Drawing the image Of Decoration
     * @param g2d
     * @param imageCreator
     */
    @Override
    public void draw(Graphics2D g2d , ImageCreator imageCreator) {
        g2d.drawImage(imageCreator.read(image),getLocX(),getLocY(),null);
    }
}
