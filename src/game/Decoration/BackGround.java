/*** In The Name of Allah ***/
package game.Decoration;

import java.util.Random;

/**
 * This Class is for Types Of BackGrounds
 * with many Pictures that we have
 * and we can draw each of them with its
 * Super Class "Decoration" ...
 *
 * @author Mazochi & Khatemi
 */
public class BackGround extends Decoration {

    public BackGround(int realX, int realY,String type,int stage) {
        super(realX, realY);
        switch (type) {
            case "Soil":
            Random random = new Random();
            int number = random.nextInt(100);

            if(stage==1){
                if (number < 50) {
                    image = "mySoil.png";
                } else if (number < 90) {
                    image = "mySoil2.png";
                } else {
                    image = "mySoil3.png";
                }
            }
            else if(stage==2){
                if (number < 50) {
                    image = "mySoilP.png";
                } else if (number < 90) {
                    image = "mySoilP2.png";
                } else {
                    image = "mySoilP3.png";
                }
            }
            else if(stage == 3){
                if (number < 50) {
                    image = "mySoilR.png";
                } else if (number < 90) {
                    image = "mySoilR2.png";
                } else {
                    image = "mySoilR3.png";
                }
            }
            break;
            case "Rail":
                if (stage == 1){
                    image = "Rail.png";
                }
                else if (stage == 2){
                    image = "RailP.png";
                }
                else if (stage == 3){
                    image = "RailR.png";
                }
                break;
            case "Granite":
                image = "Granite.png";
                break;
            case "Destroy":
                image = "Destroy.png";
                break;
            case "DestroyWall":
                if (stage==1){
                    image = "softWall4.png";
                }
                else if(stage==2){
                    image = "softWallP4.png";
                }
                else if (stage == 3){
                    image = "softWallR4.png";
                }
                break;
        }
    }
}
