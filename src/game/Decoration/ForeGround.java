/*** In The Name of Allah ***/
package game.Decoration;

import java.util.Random;

/**
 * This Class is for Types Of ForeGrounds
 * with many Pictures that we have
 * and we can draw each of them with its
 * Super Class "Decoration" ...
 *
 * @author Mazochi & Khatemi
 */
public class ForeGround extends Decoration{

    public ForeGround(int realX, int realY,String type) {
        super(realX, realY);

        switch (type) {
            case "Tree":
            Random random = new Random();
            int randomNumber = random.nextInt(3);
            if (randomNumber == 0) {
                image = "Tree3.png";
            } else if (randomNumber == 1) {
                image = "Tree2.png";
            } else {
                image = "Tree4.png";
            }
            break;
            case "VFens":
                image = "Vertical_Fens.png";
                break;
            case "HFens":
                image = "Horizental_Fens.png";
                break;
            case "ForeGroundP":
                image = "foreGroundP.png";
                break;
            case "ForeGroundR":
                image = "foreGroundR.png";
                break;
            case "Pipe1":
                image = "pipe1_FG.png";
                break;
            case "Pipe2":
                image = "pipe2_FG.png";
                break;
            case "Pipe3":
                image = "pipe3_FG.png";
                break;
            case "Pipe4":
                image = "pipe4_FG.png";
                break;
            case "Pipe5":
                image = "pipe5_FG.png";
                break;
            case "Pipe6":
                image = "pipe6_FG.png";
                break;
        }
    }
}
