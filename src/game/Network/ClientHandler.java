/*** In The Name of Allah ***/
package game.Network;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * This Class Is About Move And Attack of Client
 * With MouseListener And KeyListener
 * @author Mazochi & Khatemi
 */
public class ClientHandler {

    private KeyHandler keyHandler;
    private MouseHandler mouseHandler;

    private boolean keyUP, keyDOWN, keyRIGHT, keyLEFT;
    private boolean attack;
    private boolean changeGun;

    public ClientHandler(){


        keyHandler = new KeyHandler();
        mouseHandler = new MouseHandler();

        keyUP = false;
        keyDOWN = false;
        keyRIGHT = false;
        keyLEFT = false;

        attack=false;
        changeGun=false;
    }

    /**
     * The keyboard handler.
     */
    class KeyHandler extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {

            switch (e.getKeyCode())
            {

                case KeyEvent.VK_UP:
                    keyUP = true;
                    break;
                case KeyEvent.VK_DOWN:
                    keyDOWN = true;
                    break;
                case KeyEvent.VK_LEFT:
                    keyLEFT = true;
                    break;
                case KeyEvent.VK_RIGHT:
                    keyRIGHT = true;
                    break;
                case KeyEvent.VK_W:
                    keyUP = true;
                    break;
                case KeyEvent.VK_S:
                    keyDOWN = true;
                    break;
                case KeyEvent.VK_A:
                    keyLEFT = true;
                    break;
                case KeyEvent.VK_D:
                    keyRIGHT = true;
                    break;
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            switch (e.getKeyCode())
            {
                case KeyEvent.VK_UP:
                    keyUP = false;
                    break;
                case KeyEvent.VK_DOWN:
                    keyDOWN = false;
                    break;
                case KeyEvent.VK_LEFT:
                    keyLEFT = false;
                    break;
                case KeyEvent.VK_RIGHT:
                    keyRIGHT = false;
                    break;
                case KeyEvent.VK_W:
                    keyUP = false;
                    break;
                case KeyEvent.VK_S:
                    keyDOWN = false;
                    break;
                case KeyEvent.VK_A:
                    keyLEFT = false;
                    break;
                case KeyEvent.VK_D:
                    keyRIGHT = false;
                    break;
            }
        }

    }

    //Getters
    public boolean isKeyUP() {
        return keyUP;
    }
    public boolean isKeyDOWN() {
        return keyDOWN;
    }
    public boolean isKeyRIGHT() {
        return keyRIGHT;
    }
    public boolean isKeyLEFT() {
        return keyLEFT;
    }
    public boolean isAttack() {
        return attack;
    }
    public boolean isChangeGun() {
        return changeGun;
    }
    public Point getMouseLocation(){
        return MouseInfo.getPointerInfo().getLocation();
    }
    public KeyHandler getKeyHandler() {
        return keyHandler;
    }
    public MouseHandler getMouseHandler() {
        return mouseHandler;
    }

    /**
     * The mouse handler.
     */
    class MouseHandler extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            if(e.getButton()==MouseEvent.BUTTON1){
                attack=true;

            }

            if (e.getButton() == MouseEvent.BUTTON3) {
                changeGun = true;

            }
        }

        @Override
        public void mouseReleased(MouseEvent e){
            if(e.getButton()==MouseEvent.BUTTON1){
                attack=false;
            }

            if (e.getButton() == MouseEvent.BUTTON3) {
                changeGun = false;
            }
        }






    }

}
