/*** In The Name of Allah ***/
package game.Network;

import game.Barrier.Barrier;
import game.Basis.GameState;
import game.Basis.Trophy;
import game.Shot.PresentableShot;
import game.Decoration.Decoration;
import game.Tank.PresentableTank;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * This Class Is For the status Of "Single"
 * that get some Information that it is need
 * for showing in the picture from Server... .
 *
 * @author Mazochi & Khatemi
 */
public class Client{

    private GameState gameState;
    private ClientHandler clientHandler;

    public Client(GameState gameState, ClientHandler clientHandler){
        this.gameState=gameState;
        this.clientHandler=clientHandler;

    }

    public void updateClient(){

        try(Socket server = new Socket("192.168.2.5", 7655)) {

            InputStream in2 = server.getInputStream();
            ObjectInputStream in = new ObjectInputStream(in2);

            OutputStream out2 = server.getOutputStream();
            ObjectOutputStream out=new ObjectOutputStream(out2);

//            System.out.println(clientHandler.isKeyUP());
//            System.out.println(clientHandler.isKeyDOWN());
//            System.out.println(clientHandler.isChangeGun());
//            System.out.println(clientHandler.isAttack());


            try {

                out.writeBoolean(clientHandler.isKeyUP());
                out.writeBoolean(clientHandler.isKeyDOWN());
                out.writeBoolean(clientHandler.isKeyRIGHT());
                out.writeBoolean(clientHandler.isKeyLEFT());
                out.writeBoolean(clientHandler.isAttack());
                out.writeBoolean(clientHandler.isChangeGun());
                out.writeObject(clientHandler.getMouseLocation());

                gameState.getOwnTank().setNetwork(in.readInt(),in.readInt(),in.readInt(),in.readInt(),in.readInt(),in.readInt(),(String)in.readObject());

                gameState.setPresentableBackGrounds((ArrayList<Decoration>) in.readObject());
                gameState.setPresentableBarriers((ArrayList<Barrier>) in.readObject());
                gameState.setPresentableTanks((ArrayList<PresentableTank>) in.readObject());
                gameState.setPresentableTrophies((ArrayList<Trophy>) in.readObject());
                gameState.setPresentableShots((ArrayList<PresentableShot>) in.readObject());
                gameState.setPresentableForeGrounds((ArrayList<Decoration>) in.readObject());

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
}
