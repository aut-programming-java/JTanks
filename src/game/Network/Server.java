/*** In The Name of Allah ***/
package game.Network;

import game.Basis.GameState;
import game.Tank.OwnTank;
import java.awt.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * This Class is for "Server" status that
 * Send Necessary Information to Client
 * And Receive Some Move And Shot Of Client Player
 *
 * @author Mazochi & Khatemi
 */
public class Server{

    private GameState gameState;
    private OwnTank ownTank;

    public Server(GameState gameState,OwnTank ownTank){
        this.gameState=gameState;
        this.ownTank=ownTank;
    }


    public void setOwnTank(OwnTank ownTank) {
        this.ownTank = ownTank;
    }

    public void updateServer(){
        try (ServerSocket server = new ServerSocket(7655)) {

            try (Socket client = server.accept()) {
                OutputStream out2 = client.getOutputStream();
                ObjectOutputStream  out = new ObjectOutputStream(out2);

                InputStream in2 = client.getInputStream();
                ObjectInputStream in = new ObjectInputStream(in2);

                try {
                    boolean keyUP=in.readBoolean();
                    boolean keyDOWN=in.readBoolean();
                    boolean keyRIGHT=in.readBoolean();
                    boolean keyLEFT=in.readBoolean();
                    boolean attack=in.readBoolean();
                    boolean changeGun=in.readBoolean();
                    Point mouseLocation = (Point) in.readObject();

                    ownTank.updateNetwork(keyUP ,keyDOWN, keyRIGHT, keyLEFT, attack, changeGun, mouseLocation);

                    out.writeInt(ownTank.getNumberOfCannon());
                    out.writeInt(ownTank.getNumberOfGun());
                    out.writeInt(ownTank.getDegreeOfCanon());
                    out.writeInt(ownTank.getDegreeOfGun());
                    out.writeInt(ownTank.getLife());
                    out.writeInt(ownTank.getChance());
                    out.writeObject(ownTank.getTypeAttack());


                    out.writeObject(gameState.getPresentableBackGrounds());
                    out.writeObject(gameState.getPresentableBarriers());
                    out.writeObject(gameState.getPresentableTanks());
                    out.writeObject(gameState.getPresentableTrophies());
                    out.writeObject(gameState.getPresentableShots());
                    out.writeObject(gameState.getPresentableForeGrounds());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            } catch (IOException ex) {
                System.err.println(ex);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

